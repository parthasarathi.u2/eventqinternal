using NUnit.Framework;
using Moq;
using EventQMobileApp;
using Prism.Navigation;
using EventQMobileApp.Services.Repository;

namespace EventQ.UnitTest
{
    [TestFixture]
    public class LoginTests
    {
        LoginPageViewModel _loginPageViewModel;
        Mock<INavigationService> _navigationService;
        Mock<IDataService> _dataService;
        Mock<IUserRepository> _userRepository;

        [SetUp]
        public void Setup()
        {
            //Create the mock references
            _navigationService = new Mock<INavigationService>();
            _dataService = new Mock<IDataService>();
            _userRepository = new Mock<IUserRepository>();

            _loginPageViewModel = new LoginPageViewModel(_navigationService.Object, _userRepository.Object, _dataService.Object);

        }

        [Test]
        public void LoginCommand_Should_Call_NavigationAsync()
        {
            // Arrange Mock
            _dataService.Setup(x => x.SignInWithEmailPassword(string.Empty, string.Empty)).ReturnsAsync(true);
            _navigationService.Setup(x => x.NavigateAsync(nameof(MainPage)));

            //Act
            _loginPageViewModel.LoginCommand?.Execute();

            //Assert
            _navigationService.Verify(x => x.NavigateAsync(nameof(MainPage)));
        }
    }
}