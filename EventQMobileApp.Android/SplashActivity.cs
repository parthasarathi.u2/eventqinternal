﻿
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;

namespace EventQMobileApp.Droid
{
    [Activity(Label = "EventQ", Icon ="@drawable/logo", Theme = "@style/MyTheme.Splash",
        MainLauncher = true, LaunchMode = LaunchMode.SingleTask,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var intent = new Intent(this, typeof(MainActivity));
            StartActivity(intent);
            Finish();
        }
    }
}
