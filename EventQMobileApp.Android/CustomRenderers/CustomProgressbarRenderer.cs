﻿using System;
using Android.Content;
using EventQMobileApp;
using EventQMobileApp.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(CustomProgressbarRenderer))]
namespace EventQMobileApp.Droid.CustomRenderers
{
    public class CustomProgressbarRenderer : ProgressBarRenderer
    {
        public CustomProgressbarRenderer(Context context) :base(context)
        {
        }
        #region Protected Functions
        /// ------------------------------------------------------------------------------------------------        
        /// 
        /// 
        /// ------------------------------------------------------------------------------------------------
        /// Name		OnElementChanged
        /// 
        /// <summary>	Element Changed event
        /// </summary>
        /// ------------------------------------------------------------------------------------------------
        /// 
        protected override void OnElementChanged(ElementChangedEventArgs<ProgressBar> e)
        {
            try
            {

                base.OnElementChanged(e);
                Control.SetBackgroundColor(Android.Graphics.Color.White);
                Control.ProgressDrawable.SetColorFilter(Color.FromHex("#00cccc").ToAndroid(), Android.Graphics.PorterDuff.Mode.SrcIn);
               
            }
            catch (Exception ex)
            {
                App.Current.MainPage.DisplayAlert("Error", "Error", "OK");
                    
            }
        }
        #endregion
        /// ------------------------------------------------------------------------------------------------               

    }
}
