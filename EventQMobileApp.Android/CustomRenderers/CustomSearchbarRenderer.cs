﻿using Android.Content;
using Android.Support.V4.Content;
using EventQMobileApp;
using EventQMobileApp.Droid.Custom_controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(RoundedSearchbar), typeof(CustomSearchbarRenderer))]
namespace EventQMobileApp.Droid.Custom_controls
{
    public class CustomSearchbarRenderer : SearchBarRenderer
    {

        public CustomSearchbarRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.SetBackgroundColor(Color.FromHex("#eeeeee").ToAndroid());
                //TODO: custom_search_view Layout need to be added.
                //Control.Background = ContextCompat.GetDrawable(this.Context, Resource.Layout.custom_search_view);//Roundedn corners

                var plateId = Resources.GetIdentifier("android:id/search_plate", null, null);//Underline removal
                var plate = Control.FindViewById(plateId);
                plate.SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
            
           
        }
        
    }
}