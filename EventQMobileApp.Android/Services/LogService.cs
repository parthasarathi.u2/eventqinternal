﻿namespace EventQMobileApp.Droid.Services
{
    public class LogService : BaseLogService, ILogService
    {
        public override void Log(string message, VerbosityLevel verbosityLevel = VerbosityLevel.Debug, LogCategory logCategory = LogCategory.General)
        {
            base.Log(message, verbosityLevel, logCategory);

#if INSIGHTS
            System.Console.WriteLine($"{verbosityLevel.ToString().ToUpper()}: {message}");
#endif
        }
    }
}
