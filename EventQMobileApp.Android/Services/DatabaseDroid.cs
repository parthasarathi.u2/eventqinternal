﻿using System;
using SQLite;
using System.IO;
using Xamarin.Forms;

[assembly: Dependency (typeof(EventQMobileApp.Droid.Services.DatabaseDroid))]
namespace EventQMobileApp.Droid.Services
{
    public class DatabaseDroid : IDatabase
    {
        #region Private & Protected properties
        //----------------------------------------------------------------------------------------------------

        private string _databasePath;
        private string dbName = "EventQDatabase_Droid.db3";

        //----------------------------------------------------------------------------------------------------
        #endregion

        #region Public Methods
        //----------------------------------------------------------------------------------------------------

        public SQLiteConnection DbConnection()
        {
            _databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), dbName);
            return new SQLiteConnection(_databasePath);
        }

        public string GetSqlPath()
        {
            _databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), dbName);
            return _databasePath;
        }
        //----------------------------------------------------------------------------------------------------
        #endregion
    }
}