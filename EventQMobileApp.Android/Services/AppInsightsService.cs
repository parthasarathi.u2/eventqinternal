﻿using System;
using System.Threading.Tasks;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

namespace EventQMobileApp.Droid.Services
{
    public class AppInsightsService : IAppInsightsService
    {
        public async Task ReportAsync(Exception ex)
        {
            await Task.Run(() => Crashes.TrackError(ex));
            LogUtility.Log(ex);
        }

        public async Task TrackAsync(string message)
        {
            try
            {
                await Task.Run(() => Analytics.TrackEvent(message));
            }
            catch (Exception ex)
            {
                LogUtility.Log(ex);
            }
        }
    }
}
