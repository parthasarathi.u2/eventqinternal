﻿using System;

using Android.App;
using Android.Runtime;
using Android.OS;
using Prism;
using Prism.Ioc;
using EventQMobileApp.Droid.Services;

namespace EventQMobileApp.Droid
{
    [Activity(Label = "EventQ", Icon = "@drawable/logo", Theme = "@style/MainTheme")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            Android.Util.DisplayMetrics metrics = Resources.DisplayMetrics;
           
            LoadApplication(new App((ConvertPixelsToDp(metrics.WidthPixels)),
                (ConvertPixelsToDp(metrics.HeightPixels)),
                DeviceType.Android, GetAppBuildVersion(),
                new AndroidInitializer()));

        }

        private int ConvertPixelsToDp(float pixelValue)
        {
            return (int)((pixelValue) / Resources.DisplayMetrics.Density);
        }

        public override void OnRequestPermissionsResult(int requestCode,
            string[] permissions,
            [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public class AndroidInitializer : IPlatformInitializer
        {
            public void RegisterTypes(IContainerRegistry containerRegistry)
            {
                containerRegistry.RegisterSingleton<ILogService, LogService>();
                containerRegistry.RegisterSingleton<IAppInsightsService, AppInsightsService>();
            }
        }

        private string GetAppBuildVersion()
        {
            try
            {
                return Application.ApplicationContext.PackageManager.GetPackageInfo(Application.ApplicationContext.PackageName, 0).VersionName;
            }
            catch (Exception ex)
            {
                AppInsights.ReportAsync(ex);
            }
            return string.Empty;
        }
    }
}