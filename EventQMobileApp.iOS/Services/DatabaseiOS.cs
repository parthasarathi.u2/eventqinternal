﻿using System;
using System.IO;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(EventQMobileApp.iOS.Services.DatabaseiOS))]
namespace EventQMobileApp.iOS.Services
{
    public class DatabaseiOS : IDatabase
    {
        #region Private & Protected properties
        //----------------------------------------------------------------------------------------------------

        private string _databasePath;
        private string dbName = "EventQDatabase_iOS.db3";

        //----------------------------------------------------------------------------------------------------
        #endregion

        #region Public Methods
        //----------------------------------------------------------------------------------------------------

        public SQLiteConnection DbConnection()
        {
            _databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), dbName);
            return new SQLiteConnection(_databasePath);
        }

        public string GetSqlPath()
        {
            _databasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), dbName);
            Console.WriteLine(_databasePath);
            return _databasePath;
        }
        //----------------------------------------------------------------------------------------------------
        #endregion
    }
}
