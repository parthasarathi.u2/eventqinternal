﻿using System;
using System.Reflection;
using CoreGraphics;
using EventQMobileApp;
using EventQMobileApp.iOS.CustomRenderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(IOS_EventViewCell), typeof(CustomViewCellRenderer))]
namespace EventQMobileApp.iOS.CustomRenderer
{
    public class CustomViewCellRenderer : ViewCellRenderer
    {

        public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, reusableCell, tv);
            CGRect rect = new CGRect(0, 0, 1, 1);
            CGSize size = rect.Size;
            UIGraphics.BeginImageContext(size);
            CGContext currentNormalContext = UIGraphics.GetCurrentContext();
            currentNormalContext.SetFillColor(((Color)((IOS_EventViewCell)item).DistructiveColor).ToCGColor());
            currentNormalContext.FillRect(rect);
            var normalBackgroundImage = UIGraphics.GetImageFromCurrentImageContext();
            currentNormalContext.Dispose();

            var t = Type.GetType("Xamarin.Forms.Platform.iOS.ContextActionsCell, Xamarin.Forms.Platform.iOS, Version=1.3.1.0, Culture=neutral, PublicKeyToken=null");

            var destructive = t.GetField("DestructiveBackground", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            var normal = t.GetField("NormalBackground", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            if (normal != null)
                normal.SetValue(null, normalBackgroundImage);

            return cell;
        }
    }
}
