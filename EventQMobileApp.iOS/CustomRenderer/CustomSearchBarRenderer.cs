﻿using System;
using EventQMobileApp;
using EventQMobileApp.iOS.CustomRenderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(RoundedSearchbar), typeof(CustomSearchBarRenderer))]
namespace EventQMobileApp.iOS.CustomRenderer
{
    public class CustomSearchBarRenderer : SearchBarRenderer
    {

        /// ------------------------------------------------------------------------------------------------
        #region Protected  Methods
        ///
        /// ------------------------------------------------------------------------------------------------
        /// Name		OnElementChanged
        /// 
        /// <summary>	Element Changed event
        /// </summary>
        /// ------------------------------------------------------------------------------------------------
        /// 
        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);

            var searchbar = (UISearchBar)Control;
            var searchBarStyle = (RoundedSearchbar)e.NewElement;
            if (e.NewElement != null)
            {
                searchbar.SearchBarStyle = UISearchBarStyle.Minimal;
                searchbar.Layer.CornerRadius = searchBarStyle.CornerRadius;//20
                searchbar.Layer.BorderWidth = searchBarStyle.BorderWidth;//14
                searchbar.Layer.BorderColor = UIColor.FromRGB(searchBarStyle.BorderColorRed, searchBarStyle.BorderColorGreen,
                    searchBarStyle.BorderColorBlue).CGColor;
                searchbar.BackgroundColor = UIColor.White;
               
            }
        }
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "Text")
            {
                Control.ShowsCancelButton = false;
            }
        }
        public override CoreGraphics.CGSize SizeThatFits(CoreGraphics.CGSize size)
        {
            return new CoreGraphics.CGSize(20, 20);
        }
        #endregion
        /// ------------------------------------------------------------------------------------------------
    }
}
