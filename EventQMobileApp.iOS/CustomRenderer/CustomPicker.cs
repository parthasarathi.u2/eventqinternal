﻿using System;
using EventQMobileApp;
using EventQMobileApp.iOS.CustomRenderer;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(EQPicker), typeof(CustomPicker))]
namespace EventQMobileApp.iOS.CustomRenderer
{
    public class CustomPicker : PickerRenderer
    {
        public CustomPicker()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Picker> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                Control.Font = UIFont.FromName("CustomFont", 4f);

                UIFont font = Control.Font.WithSize(18);
                Control.Font = font;
                Control.InputAssistantItem.LeadingBarButtonGroups = null;
                Control.InputAssistantItem.TrailingBarButtonGroups = null;
            }
        }
    }
}
