﻿using System;
using System.Threading.Tasks;
using CoreGraphics;
using EventQMobileApp;
using EventQMobileApp.iOS.CustomRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(CustomProgressBarRenderer))]
namespace EventQMobileApp.iOS.CustomRenderer
{


    public class CustomProgressBarRenderer : ProgressBarRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.ProgressTintColor = Color.FromHex("#00cccc").ToUIColor();
            }

        }

    }
}