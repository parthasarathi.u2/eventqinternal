﻿using System;

namespace EventQMobileApp
{
    public class FormatAttribute : Attribute
    {
        public FormatAttribute(string format)
        {
            Format = format;
        }

        public string Format { get; set; }
    }
}
