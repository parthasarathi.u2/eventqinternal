﻿using System;

namespace EventQMobileApp
{
    [AttributeUsage(AttributeTargets.Field)]
    public class Description : Attribute
    {
        readonly string _name;
        readonly bool _isDefault;
        readonly bool _canDisplay;

        public string Name { get { return _name; } }

        public bool IsDefault { get { return _isDefault; } }

        public bool CanDisplay { get { return _canDisplay; } }

        public Description(string name, bool isDefault = false, bool canDisplay = true)
        {
            _name = name;
            _isDefault = isDefault;
            _canDisplay = canDisplay;
        }
    }
}
