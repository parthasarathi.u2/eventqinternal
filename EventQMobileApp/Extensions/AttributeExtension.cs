﻿using System;
using System.Linq;
using System.Reflection;

namespace EventQMobileApp
{
    public static class AttributeExtension
    {
        /// <summary>
        /// Gets an attribute on an enum field value
        /// </summary>
        /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
        /// <param name="enumVal">The enum value</param>
        /// <returns>The attribute of type T that exists on the enum value</returns>
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : Attribute
        {
            if (enumVal == null)
            {
                return null;
            }
            var fi = enumVal.GetType().GetRuntimeField(enumVal.ToString());
            var attributes = fi.GetCustomAttributes(typeof(T), false);

            if (attributes != null && attributes.Count() > 0)
            {
                return ((T)attributes.ElementAt(0));
            }
            else
            {
                return null;
            }
        }
    }
}
