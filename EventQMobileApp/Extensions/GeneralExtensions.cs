﻿namespace EventQMobileApp
{
    public static class GeneralExtensions
    {
        public static bool IsNullOrDefault<T>(this T value)
        {
            return (value == null || value.Equals((default(T))));
        }
    }
}
