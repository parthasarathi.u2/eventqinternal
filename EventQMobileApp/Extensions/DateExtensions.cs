﻿using System;
using System.Globalization;

namespace EventQMobileApp
{
    public enum DateTimeFormat
    {
        /// <summary>
        /// dd-MMM
        /// </summary>
        [Format("dd MMM")]
        ShortDate,
        /// <summary>
        /// dd-MMM-yyyy 
        /// </summary>
        [Format("dd-MMM-yyyy")]
        Date,

        /// <summary>
        /// dd/MM/yyyy HH:mm:ss
        /// </summary>
        [Format("dd/MM/yyyy HH:mm:ss")]
        DateTime,

        /// <summary>
        /// yyyy-MM-ddzzz
        /// </summary>
        [Format("yyyy-MM-ddzzz")]
        DateWithTimeZone,
        /// <summary>
        /// dd MMM hh:mm tt
        /// </summary>
        [Format("dd MMM hh:mm tt")]
        DateTimeWithTimeZone,
        /// <summary>
        /// dd MMMM yyyy
        /// </summary>
        [Format("dd MMM yyyy")]
        ShortDateTime,
        /// <summary>
        /// The iso format.
        /// </summary>
        [Format("yyyy-MM-ddTHH:mm:ssZ")]
        Iso8601Format

    }

    public static class DateExtensions
    {
        public static string ToFormattedDateTime(this DateTime? date, DateTimeFormat format = DateTimeFormat.Date, string nullValue = "")
        {
            return date.GetValueOrDefault().ToFormattedDateTime(format, nullValue);
        }

        public static string ToFormattedDateTime(this DateTime date, DateTimeFormat format = DateTimeFormat.Date, string nullValue = "")
        {
            return date.IsNullOrDefault() ? nullValue : date.ToString(format.ToFormat(), CultureInfo.InvariantCulture);
        }

        public static string ToFormattedDateTime(this DateTimeOffset? date, DateTimeFormat format = DateTimeFormat.Date, string nullValue = "")
        {
            return date.GetValueOrDefault().ToFormattedDateTime(format, nullValue);
        }

        public static string ToFormattedDateTime(this DateTimeOffset date, DateTimeFormat format = DateTimeFormat.Date, string nullValue = "")
        {
            return date.IsNullOrDefault() ? nullValue : date.ToString(format.ToFormat(), CultureInfo.InvariantCulture);
        }

        public static DateTime ToDateTime(this string date, DateTimeFormat format = DateTimeFormat.Date)
        {
            return DateTime.ParseExact(date, format.ToFormat(), CultureInfo.InvariantCulture);
        }

        public static string ToClockHours(this string twentyFourHourFormat)
        {
            DateTime date;
            // Note: I suspect this will need culture info and TryParseExact as well in the future to prevent issues but I don't know how this is used currently. Matt 16-Oct-2017.
            if (DateTime.TryParse(twentyFourHourFormat, out date))
            {
                return date.ToString("hh:mm tt", CultureInfo.InvariantCulture);
            }
            return twentyFourHourFormat;
        }

        public static string ToClockHours(this DateTimeOffset date)
        {
            return date.ToLocalTime().ToString("hh:mm tt", CultureInfo.InvariantCulture);
        }

        public static string ToClockHours(this DateTime date)
        {
            return date.ToString("hh:mm tt", CultureInfo.InvariantCulture);
        }

        public static string Humanize(this DateTimeOffset dateTime)
        {
            if (dateTime == DateTimeOffset.MinValue)
                return string.Empty;

            var localizedDateTime = dateTime.ToLocalTime();
            if (localizedDateTime > DateTime.Today)
                return $"Today {dateTime.ToClockHours()}";
            else if (localizedDateTime > DateTime.Today.AddDays(-1))
                return $"Yesterday {dateTime.ToClockHours()}";
            else if (localizedDateTime > DateTime.Today.AddDays(-2))
                return localizedDateTime.DayOfWeek.ToString();
            else
                return localizedDateTime.ToFormattedDateTime(DateTimeFormat.ShortDateTime);
        }
    }
}
