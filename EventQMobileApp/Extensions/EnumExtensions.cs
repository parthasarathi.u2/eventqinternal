﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EventQMobileApp
{
    public static class EnumExtensions
    {
        public static Enum GetEnumFromString<T>(string stringEnum)
        {
            foreach (Enum value in Enum.GetValues(typeof(T)))
            {
                if (value.GetDescription() == stringEnum)
                {
                    return value;
                }
            }
            return null;
        }

        public static List<string> GetListOfEnumValues<T>()
        {
            var values = new List<string>();
            foreach (Enum value in Enum.GetValues(typeof(T)))
            {
                if (value.GetDescription() != null)
                {
                    values.Add(value.GetDescription());
                }
                else
                {
                    values.Add(value.ToString());
                }
            }
            return values;
        }

        public static IEnumerable<int> GetIntValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<int>();
        }

        public static string GetDescription(this Enum enumVal)
        {
            return enumVal.GetAttributeOfType<Description>()?.Name;
        }

        /// <summary>
        /// Returns the description of the enum with values supplied substituted into the string
        /// </summary>
        /// <returns>The description.</returns>
        /// <param name="enumVal">Enum value.</param>
        /// <param name="values">Values.</param>
		public static string GetDescription(this Enum enumVal, params string[] values)
        {
            return string.Format(enumVal.GetAttributeOfType<Description>()?.Name, values);
        }

        public static List<string> ToDescriptionList(this Enum[] enumArray)
        {
            return enumArray.Select(x => x.GetDescription()).ToList();
        }

        public static string ToFormat(this Enum value)
        {
            var attribute = value.GetAttributeOfType<FormatAttribute>();
            return attribute == null ? value.ToString() : attribute.Format;
        }

    }
}
