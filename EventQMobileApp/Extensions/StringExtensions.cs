﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace EventQMobileApp
{
    public static class StringExtensions
    {
        // Matches anything not A-Z, a-z or 0-9 in the string
        public static Regex RegexAlphaNumeric = new Regex(@"[^[A-Za-z0-9]");

        public static Regex RegexAlphaNumericWhitespace = new Regex(@"[^[A-Za-z0-9\s]");

        public static Regex RegexUTFCharactersRegex = new Regex(@"[^\u0000-\u007F]");


        static Regex _nameRegEx = new Regex("^[\\p{L}\\s'`_.-]+$");

        static Regex _emailRegEx = new Regex("^([\\w\\.\\-]+)@([\\w\\-]+)((\\.(\\w){2,3})+)$");


        public static bool EqualsIgnoreCase(this string value, string other)
        {
            return string.Equals(value, other, StringComparison.CurrentCultureIgnoreCase);
        }

        public static bool StartsWithIgnoreCase(this string text, string startsWith)
        {
            return text != null
                   && text.StartsWith(startsWith, StringComparison.CurrentCultureIgnoreCase);
        }

        public static bool EndsWithIgnoreCase(this string text, string endsWith)
        {
            return text != null
                   && text.EndsWith(endsWith, StringComparison.CurrentCultureIgnoreCase);
        }

        public static bool ContainsIgnoreCase(this string text, string substring)
        {
            return text != null
                   && text.IndexOf(substring, StringComparison.CurrentCultureIgnoreCase) > -1;
        }

        public static bool IsNullOrEmpty(this string value)
        {
            return string.IsNullOrEmpty(value);
        }

        public static bool IsNullOrWhiteSpace(this string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }

        public static string FormatWith(this string text, params object[] args)
        {
            return string.Format(text, args);
        }

        /// <summary>
        /// Returns string representation of the byte array using UTF8.
        /// </summary>
        /// <param name="byteArray"></param>
        /// <returns></returns>
        public static string AsString(this byte[] byteArray)
        {
            return Encoding.UTF8.GetString(byteArray, 0, byteArray.Length);
        }

        /// <summary>
        /// Returns the string as bytes based on UTF8
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static byte[] AsBytes(this string stringValue)
        {
            return Encoding.UTF8.GetBytes(stringValue);
        }

        /// <summary>
        /// Returns true if the string is completely alpha numeric, according to ASCII specificiation.
        /// </summary>
        /// <param name="value">String to check</param>
        /// <returns>True if string is completely alphanumeric, otherwise false.</returns>
        public static bool IsAlphaNumeric(this string value, bool allowWhiteSpace = false)
        {
            // If we're allowing whitespace use modified pattern
            var regexPattern = (allowWhiteSpace) ? RegexAlphaNumericWhitespace : RegexAlphaNumeric;
            return regexPattern.IsMatch(value);
        }


        public static string ReplaceNonUTF8Characters(this string input)
        {
            //For invalid characters, if the user has:
            //· any characters in an address field which fall outside the UTF-8 character set,

            return RegexUTFCharactersRegex.Replace(input, "*");
        }

        public static bool HasNonUTF8Characters(this string input)
        {
            //For invalid characters, if the user has:
            //· any characters in an address field which fall outside the UTF-8 character set,
            return RegexUTFCharactersRegex.IsMatch(input);
        }


        public static string ExtractFirstLine(this string input)
        {
            //get single line of text from multi-line strings
            return input.Split(new char[] { '\n', '\r' })[0];
        }

        public static bool IsValidName(this string name)
        {
            if (name.IsNullOrWhiteSpace())
                return false;

            return _nameRegEx.IsMatch(name);
        }

        public static bool IsValidEmail(this string email)
        {
            if (email.IsNullOrWhiteSpace())
                return false;
            return _emailRegEx.IsMatch(email);
        }

        public static bool IsValidPassword(this string password)
        {
            if (password.IsNullOrWhiteSpace())
                return false;

            if (password.Length < 6)
                return false;

            return true;
        }
    }
}
