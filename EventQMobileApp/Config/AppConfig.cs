namespace EventQMobileApp
{
    public static class AppConfig
    {
        public static string APP_VERSION { get; internal set; }

        public static int SCREEN_WIDTH { get; internal set; }

        public static int SCREEN_HEIGHT { get; internal set; }

        public static DeviceType DEVICE_TYPE { get; set; }

        public static string STANDARD_ERROR_MESSAGE => "Oops! Something went wrong.";

#if RELEASE
        public static string API_URL = "";
        public static string WEB_URL = "";

#elif QMEETO_DEV
        public static string API_URL = "";
        public static string WEB_URL = "";
#else
        public static string API_URL = "";
        //public static string API_URL = "http://10.72.23.234/";
        public static string WEB_URL = "";
#endif
        public static string APP_NAME = "EventQ";
        public static string VALIDATION = "qmeetoApi/accountLogin";
        public static string FETCH_ATTENDEES = "api/Attendee/GetEventDetails/{0}/{1}";
        public static string FETCH_EVENTS = "api/Event/GetEvents/{0}/{1}";

        public static void Init(int deviceWidth, int deviceHeight, DeviceType deviceType, string appVersion)
        {
            SCREEN_WIDTH = deviceWidth;
            SCREEN_HEIGHT = deviceHeight;
            DEVICE_TYPE = deviceType;
            APP_VERSION = appVersion;
        }
    }
}
