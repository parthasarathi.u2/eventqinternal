﻿using System;
using AutoMapper;

namespace EventQMobileApp
{
    public class AutoMappingBootstrapper : IAutoMapper
    {
        public IMapper Mapper { get; private set; }
        public AutoMappingBootstrapper()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<Attendee, AttendeeBaseViewModel>();
                });
            Mapper = config.CreateMapper();
        }

        public IMapper GetMapper()
        {
            return Mapper;
        }
    }
}
