﻿using System;

namespace EventQMobileApp
{
    public static class Identity
    {
        private static double _expiryMinutes = 59;

        private static string _idToken;
        public static string IdToken
        {
            get => _idToken;
            set
            {
                _idToken = value;
                TokenExpiry = DateTime.Now.AddMinutes(_expiryMinutes);
            }
        }

        public static DateTime TokenExpiry { get; private set; }

        public static string RefreshToken { get; set; }

        public static string UserID { get; set; }

        public static User User { get; set; }

        public static string DeviceID { get; set; }

        public static string PushNotificationID { get; set; }

        public static DeviceType DeviceType { get; set; }

        public static string GenerateDeviceID()
        {
            return Guid.NewGuid().ToString();
        }
    }
}
