﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EventQMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Droid_EventViewCell : ViewCell
    {
        private SwipeDirection swipeDirection = SwipeDirection.None;
        public bool isSwipped;
        public EventHandler<SwippedItemEventArgs> ItemSwipped;

        public Droid_EventViewCell()
        {
            InitializeComponent();
        }

        private void SyncMenu_Clicked(object sender, EventArgs e)
        {
            var viewModel = (ActiveEventsPageViewModel)ParentBindingContext;
            if ((viewModel != null) && viewModel.SyncMenuCommand.CanExecute((Event)BindingContext))
            {
                viewModel.SyncMenuCommand.Execute((Event)BindingContext);
            }
        }

        public static BindableProperty ParentBindingContextProperty = BindableProperty.Create(nameof(ParentBindingContext),
            typeof(object), typeof(Droid_EventViewCell), null);

        public object ParentBindingContext
        {
            get { return GetValue(ParentBindingContextProperty); }
            set { SetValue(ParentBindingContextProperty, value); }
        }


        private void OnMenuItemTapped(object sender, EventArgs e)
        {
            var viewModel = (EventViewCellViewModel)BindingContext;
            if (viewModel.MenuItemTappedCommand.CanExecute())
            {
                viewModel.MenuItemTappedCommand.Execute();
            }
            Application.Current.MainPage.DisplayAlert("Alert", "SignOutTapped", "OK");
        }

        private async void OnPanUpdated(Object sender, PanUpdatedEventArgs e)
        {
            var topView = (Grid)sender;
            if (topView.Parent != null && topView.Parent is Grid innerView)
            {
                switch (e.StatusType)
                {
                    case GestureStatus.Running:
                        swipeDirection = topView.X < e.TotalX ? SwipeDirection.Right : SwipeDirection.Left;

                        await topView.TranslateTo(e.TotalX, 0, 50, Easing.Linear);
                        if (e.TotalX <= 0)
                            innerView.ColumnDefinitions[1].Width = new GridLength(Math.Abs(e.TotalX), GridUnitType.Absolute);
                        break;
                    case GestureStatus.Completed:
                        if (!isSwipped && swipeDirection == SwipeDirection.Left)
                        {
                            await topView.TranslateTo(-100, 0, 100, Easing.SinIn);
                            innerView.ColumnDefinitions[1].Width = new GridLength(100, GridUnitType.Absolute);
                            isSwipped = true;
                            ItemSwipped?.Invoke(this, new SwippedItemEventArgs(sender, swipeDirection));
                        }
                        else
                        {
                            await topView.TranslateTo(0, 0, 100, Easing.SinIn);
                            innerView.ColumnDefinitions[1].Width = new GridLength(0, GridUnitType.Absolute);
                            isSwipped = false;
                        }
                        break;
                }
            }
        }

        public async void CloseSwipe(object sender)
        {
            var topView = (Grid)sender;
            await topView.TranslateTo(0, 0, 100, Easing.SinIn);
            ((Grid)topView.Parent).ColumnDefinitions[1].Width = new GridLength(0, GridUnitType.Absolute);
        }

        private async void OnItemTapped(object sender, EventArgs e)
        {
            try
            {
                await Task.WhenAll(
                     MainGrid.ScaleTo(1.3, 200, Easing.Linear),
                     MainGrid.FadeTo(0, 100, Easing.Linear)
                    );

                MainGrid.Scale = 1;
                await MainGrid.FadeTo(1, 0);
                var viewModel = (EventViewCellViewModel)BindingContext;
                if (viewModel.ItemTappedCommand.CanExecute())
                {
                    viewModel.ItemTappedCommand.Execute();

                }
            }
            catch (Exception ex)
            {
                LogUtility.Log(ex);
            }
        }

    }
}
