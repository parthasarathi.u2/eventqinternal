﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EventQMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IOS_EventViewCell : ViewCell
    {
        public IOS_EventViewCell()
        {
            InitializeComponent();
            View.BackgroundColor = Color.White;
        }

        private void SyncMenu_Clicked(object sender, EventArgs e)
        {
            var viewModel = (ActiveEventsPageViewModel)ParentBindingContext;
            if ((viewModel != null) && viewModel.SyncMenuCommand.CanExecute((Event)BindingContext))
            {
                viewModel.SyncMenuCommand.Execute((Event)BindingContext);
            }
        }

        public static BindableProperty ParentBindingContextProperty = BindableProperty.Create(nameof(ParentBindingContext),
            typeof(object), typeof(IOS_EventViewCell), null);

        public object ParentBindingContext
        {
            get { return GetValue(ParentBindingContextProperty); }
            set { SetValue(ParentBindingContextProperty, value); }
        }

        public static BindableProperty DestructiveColorProperty = BindableProperty.Create(nameof(DistructiveColor),
            typeof(Color), typeof(IOS_EventViewCell), Color.Green);

        public object DistructiveColor
        {
            get { return GetValue(DestructiveColorProperty); }
            set { SetValue(DestructiveColorProperty, value); }
        }
    }
}
