﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EventQMobileApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AttendeeViewCell : BaseViewCell
    {
        public AttendeeViewCell()
        {
            InitializeComponent();
        }

        async void OnCheckIn(object sender, System.EventArgs e)
        {
            var menuItem = ((MenuItem)sender);
            int rotate;
            uint timing;

            if (((AttendeeBaseViewModel)menuItem.CommandParameter).IsEntranceDone)
            {
                rotate = 360;
                timing = 600;
            }
            else
            {
                rotate = -180;
                timing = 300;
            }
            await Task.WhenAll(
                    timeLayout.ScaleTo(1.2, 250),
                    timeLayout.RotateYTo(rotate, timing, Easing.Linear)
                   );
            await timeLayout.ScaleTo(1, 250, Easing.Linear);
            await timeLayout.RotateYTo(0, 0);
        }

        private void OnItemTapped(object sender, EventArgs args)
        {
            ((ViewCell)sender).View.BackgroundColor = (Color)Application.Current.Resources["ViewCellBackgrounColor"];
        }
    }
}
