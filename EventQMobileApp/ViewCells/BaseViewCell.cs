﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class BaseViewCell : ViewCell
    {

        protected virtual async void MenuItem_Clicked(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                try
                {
                    if (sender is MenuItem menuItem)
                    {
                        //if (menuItem.CommandParameter is Notification notification)
                        //{
                        //    if (notification == null)
                        //        return;

                        //    if (menuItem.Text == "Decline")
                        //    {
                        //        if (notification.HyerInfo == null)
                        //            return;

                        //        int hyerId = notification.HyerInfo.HyerID;
                        //        var dataService = AppServiceLocator.Resolve<IDataService>();
                        //        if (dataService != null)
                        //        {
                        //            var result = await dataService.DeclineHyerRequest(hyerId);
                        //        }


                        //    }
                        //    else if (menuItem.Text == "Hide")
                        //    {
                        //        var dataService = AppServiceLocator.Resolve<IDataService>();
                        //        if (dataService != null)
                        //        {
                        //            var result = await dataService.HideNotification(notification.ID);
                        //        }
                        //    }
                        //}
                    }
                }
                catch (Exception ex)
                {
                    await AppInsights.ReportAsync(ex);
                }
            });
        }
    }
}
