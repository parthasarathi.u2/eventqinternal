﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public partial class ImageTextButton : ContentView
    {
        bool isAnimating;
        public string IconSource
        {
            get => (string)GetValue(IconSourceProperty);
            set => SetValue(IconSourceProperty, value);
        }

        public static BindableProperty IconSourceProperty = BindableProperty.Create(nameof(IconSource),
            typeof(string), typeof(PopupEntryView), string.Empty,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (ImageTextButton)bindable;
                if (oldVal == null && newVal == null)
                {
                    return;
                }

                if (newVal != null)
                {
                    view.icon.Source = (string)newVal;

                }
            });


        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public static BindableProperty TextProperty = BindableProperty.Create(nameof(Text),
            typeof(string), typeof(PopupEntryView), string.Empty,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (ImageTextButton)bindable;
                if (oldVal == null && newVal == null)
                {
                    return;
                }

                if (newVal != null)
                {
                    view.labelText.Text = (string)newVal;
                }
            });

        public bool Animate
        {
            get => (bool)GetValue(AnimateProperty);
            set => SetValue(AnimateProperty, value);
        }

        public static BindableProperty AnimateProperty = BindableProperty.Create(nameof(Animate),
            typeof(bool), typeof(PopupEntryView), false,
            propertyChanged: async (bindable, oldVal, newVal) =>
            {
                var view = (ImageTextButton)bindable;
                if (oldVal == null && newVal == null)
                {
                    return;
                }

                if (newVal != null)
                {
                    while (view.Animate)
                    {
                        await view.icon.RotateTo(360, 800, Easing.Linear);
                        await view.icon.RotateTo(0, 0); // reset to initial position
                    }
                    await view.icon.RotateTo(0, 0);
                }
            });


        public ICommand Command
        {
            get => (ICommand)GetValue(CommandProperty);
            set => SetValue(CommandProperty, value);
        }

        public static readonly BindableProperty CommandProperty = BindableProperty.Create<ImageTextButton, ICommand>(x => x.Command, null);


        public ImageTextButton()
        {
            InitializeComponent();
        }

        public async void ButtonTapped(object sender, EventArgs e)
        {
            if (Command != null && Command.CanExecute(e))
            {
                if (Text == EventQMobileApp.Resources.AppText.WalkIn)
                {
                    await icon.RotateYTo(180, 500);
                    await icon.RotateYTo(0, 500);
                }
                Command.Execute(e);
            }
        }
    }
}
