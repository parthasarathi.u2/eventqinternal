﻿using System;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class EQPicker : Picker
    {
        public EQPicker()
        {
            Focused += (sender, e) => BackgroundColor = (Color)Application.Current.Resources["ThemeColor"];
            Unfocused += (sender, e) => BackgroundColor = (Color)Application.Current.Resources["BoxviewColor"];

        }
    }
}
