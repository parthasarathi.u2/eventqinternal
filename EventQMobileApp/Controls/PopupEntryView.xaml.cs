﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public partial class PopupEntryView : ContentView
    {
        public string PlaceHolderText
        {
            get => (string)GetValue(PlaceHolderTextProperty);
            set => SetValue(PlaceHolderTextProperty, value);
        }

        public static BindableProperty PlaceHolderTextProperty = BindableProperty.Create(nameof(PlaceHolderText),
            typeof(string), typeof(PopupEntryView), string.Empty,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (PopupEntryView)bindable;
                if (oldVal == null && newVal == null)
                {
                    view.IsVisible = false;
                    return;
                }

                view.IsVisible = newVal != null && !newVal.ToString().IsNullOrEmpty();
                if (newVal != null)
                {
                    view.entryPopup.Placeholder = (string)newVal;

                }
            });


        public bool IsMandatory
        {
            get => (bool)GetValue(IsMandatoryProperty);
            set => SetValue(IsMandatoryProperty, value);
        }

        public static BindableProperty IsMandatoryProperty = BindableProperty.Create(nameof(IsMandatory),
            typeof(bool), typeof(PopupEntryView), false,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (PopupEntryView)bindable;
                if (oldVal == null && newVal == null)
                {
                    view.IsVisible = false;
                    return;
                }

                if (newVal != null)
                {
                    view.labelMandatory.IsVisible = (bool)newVal;
                }
            });

        public PopupEntryView()
        {
            InitializeComponent();
        }
    }
}
