﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public partial class EditorEntryView : ContentView
    {
        public string TitleText
        {
            get => (string)GetValue(TitleTextProperty);
            set => SetValue(TitleTextProperty, value);
        }

        public static BindableProperty TitleTextProperty = BindableProperty.Create(nameof(TitleText),
            typeof(string), typeof(EditorEntryView), string.Empty,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (EditorEntryView)bindable;
                if (oldVal == null && newVal == null)
                {
                    view.IsVisible = false;
                    return;
                }

                view.IsVisible = newVal != null && !newVal.ToString().IsNullOrEmpty();
                if (newVal != null)
                {
                    view.labelTitle.Text = (string)newVal;

                }
            });

        public string UserData
        {
            get => (string)GetValue(UserDataProperty);
            set => SetValue(UserDataProperty, value);
        }

        public static BindableProperty UserDataProperty = BindableProperty.Create(nameof(UserData),
            typeof(string), typeof(EditorEntryView), string.Empty, BindingMode.TwoWay,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (EditorEntryView)bindable;
                if (oldVal == null && newVal == null)
                {
                    view.IsVisible = false;
                    return;
                }
                view.labelUserData.Text = view.entryEditable.Text = (string)newVal;
                view.entryEditable.IsVisible = false;
            });

        public bool IsEditable
        {
            get => (bool)GetValue(IsEditableProperty);
            set => SetValue(IsEditableProperty, value);
        }

        public static BindableProperty IsEditableProperty = BindableProperty.Create(nameof(IsEditable),
            typeof(bool), typeof(EditorEntryView), false,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (EditorEntryView)bindable;
                if (oldVal == null && newVal == null)
                {
                    view.IsVisible = false;
                    return;
                }

                if (newVal != null)
                {
                    view.UserData = view.entryEditable.Text;
                    view.entryEditable.IsVisible = (bool)newVal;
                }
            });

        public bool IsMandatory
        {
            get => (bool)GetValue(IsMandatoryProperty);
            set => SetValue(IsMandatoryProperty, value);
        }

        public static BindableProperty IsMandatoryProperty = BindableProperty.Create(nameof(IsMandatory),
            typeof(bool), typeof(EditorEntryView), false,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (EditorEntryView)bindable;
                if (oldVal == null && newVal == null)
                {
                    view.IsVisible = false;
                    return;
                }

                if (newVal != null && (bool)newVal)
                {
                    view.labelMandatory.IsVisible = true;
                }
            });

        public EditorEntryView()
        {
            InitializeComponent();
        }
    }
}
