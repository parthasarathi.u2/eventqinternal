﻿using System;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class RoundedSearchbar : SearchBar
    {
        public int Radius
        {
            get;
            set;
        }
        public int CornerRadius
        {
            get;
            set;
        }
        public int BorderWidth
        {
            get;
            set;
        }
        public int BorderColorRed
        {
            get;
            set;
        }
        public int BorderColorGreen
        {
            get;
            set;
        }
        public int BorderColorBlue
        {
            get;
            set;
        }
    }
}
