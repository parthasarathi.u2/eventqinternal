﻿using Xamarin.Forms;

namespace EventQMobileApp
{
    public class FullStackLayout : StackLayout
    {
        public FullStackLayout()
        {
            Padding = new Thickness(0);
            HeightRequest = AppConfig.SCREEN_HEIGHT - 150;
        }
    }
}
