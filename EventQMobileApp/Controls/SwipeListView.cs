﻿using System;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class SwipeListView : ListView
    {
        EventHandler<ItemTappedEventArgs> ItemSwipped;

        public SwipeListView()
        {
            ItemSwipped += OnItemSwiped;
            
        }

        private void OnItemSwiped(object sender, ItemTappedEventArgs e)
        {

        }
    }
}
