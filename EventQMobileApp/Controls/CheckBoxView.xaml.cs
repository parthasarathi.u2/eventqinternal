﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public partial class CheckBoxView : ContentView
    {

        public string LabelText
        {
            get => (string)GetValue(LabelTextProperty);
            set => SetValue(LabelTextProperty, value);
        }

        public static BindableProperty LabelTextProperty = BindableProperty.Create(nameof(LabelText),
            typeof(string), typeof(CheckBoxView), string.Empty,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (CheckBoxView)bindable;
                if (oldVal == null && newVal == null)
                {
                    view.IsVisible = false;
                    return;
                }

                if (newVal != null)
                {
                    view.labelCheckBoxName.Text = (string)newVal;

                }
            });

        public bool IsChecked
        {
            get => (bool)GetValue(IsCheckedProperty);
            set => SetValue(IsCheckedProperty, value);
        }

        public static BindableProperty IsCheckedProperty = BindableProperty.Create(nameof(IsChecked),
            typeof(bool), typeof(CheckBoxView), false, BindingMode.TwoWay,
            propertyChanged: (bindable, oldVal, newVal) =>
            {
                var view = (CheckBoxView)bindable;
                if (oldVal == null && newVal == null)
                {
                    view.IsVisible = false;
                    return;
                }

                if (newVal != null)
                {
                    view.imageCheckBox.Source = (bool)newVal ? Constant.CHECKED_IMAGE_SOURCE : Constant.UNCHECKED_IMAGE_SOURCE;

                }
            });

        public CheckBoxView()
        {
            InitializeComponent();
        }

        void OnImageCheckBox_Clicked(object sender, System.EventArgs e)
        {
            this.IsChecked = !IsChecked;
        }
    }
}
