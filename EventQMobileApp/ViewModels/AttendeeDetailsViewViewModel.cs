﻿using System;
using Prism;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class AttendeeDetailsViewViewModel : AttendeeBaseViewModel
    {

        string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        string _email;
        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        string _contactType;
        public string ContactType
        {
            get => _contactType;
            set => SetProperty(ref _contactType, value);
        }

        bool _isEditable;
        public bool IsEditable
        {
            get => _isEditable;
            set => SetProperty(ref _isEditable, value);
        }

        bool _isEditButtonEnabled;
        public bool IsEditButtonEnabled
        {
            get => _isEditButtonEnabled;
            set => SetProperty(ref _isEditButtonEnabled, value);
        }

        string _status;
        public string Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }

        Status _attendeeStatus;
        public Status EntranceStatus
        {
            get => _attendeeStatus;
            set
            {
                SetProperty(ref _attendeeStatus, value);
                if (_attendeeStatus == EventQMobileApp.Status.Checked_In)
                    Status = string.Format("{0} @ {1}", _attendeeStatus.GetDescription(), CheckInTime?.ToString("hh:mm tt"));
                else
                    Status = _attendeeStatus.GetDescription();
            }
        }

        string _attendeeLable;
        public string AttendeeLabel

        {
            get => _attendeeLable;
            set => SetProperty(ref _attendeeLable, value);
        }

        DelegateCommand _editCommand;
        public DelegateCommand EditCommand => _editCommand ?? (_editCommand = new DelegateCommand(async () =>
       {
           IsEditable = !IsEditable;
           MessagingCenter.Send<AttendeeDetailsViewViewModel, AttendeeBaseViewModel>(this, MessagingCenterValues.Edit.GetDescription(), this);
       }));

        public AttendeeDetailsViewViewModel(INavigationService navigationService)
        {

            MessagingCenter.Subscribe<BasePageViewModel, Attendee>(this, MessagingCenterValues.AttendeeModel.GetDescription(),
                (sender, attendee) =>
                {
                    if (attendee != null && attendee is Attendee baseAttendee)
                    {
                        this.AttendeeId = attendee.AttendeeId;
                        this.Title = baseAttendee.Title;
                        this.FirstName = baseAttendee.FirstName;
                        this.SurName = baseAttendee.SurName;
                        this.AttendeeType = baseAttendee.AttendeeType;
                        this.IsVip = baseAttendee.IsVip;
                        this.Email = baseAttendee.Email;
                        this.Organisation = baseAttendee.Organisation;
                        this.AttendeeLabel = baseAttendee.AttendeeLabel;
                        this.CheckInTime = baseAttendee.CheckInTime;
                        this.EntranceStatus = GetStatus(baseAttendee.IsEntranceDone, baseAttendee.CheckInTime);
                    }

                });

            IsEditButtonEnabled = true;
        }

        private Status GetStatus(bool isEntranceDone, DateTime? checkedInTime = null)
        {
            Status attendeeStatus;
            if (isEntranceDone)
            {
                attendeeStatus = EventQMobileApp.Status.Checked_In;
            }
            else
            {
                attendeeStatus = EventQMobileApp.Status.Not_Checked_In_Yet;
            }
            if (isEntranceDone && checkedInTime == null)
            {
                attendeeStatus = EventQMobileApp.Status.Time_Not_Available;
            }
            return attendeeStatus;
        }

        ~AttendeeDetailsViewViewModel()
        {
            MessagingCenter.Unsubscribe<BasePageViewModel, Attendee>(this, MessagingCenterValues.AttendeeModel.GetDescription());
        }
    }
}
