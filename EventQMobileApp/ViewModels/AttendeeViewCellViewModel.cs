﻿using System;
using Prism.Navigation;

namespace EventQMobileApp
{
    public class AttendeeViewCellViewModel : BasePageViewModel
    {
        DateTime _checkInTime;
        public DateTime CheckInTime
        {
            get => _checkInTime;
            set => SetProperty(ref _checkInTime, value);
        }

        string _surName;
        public string SurName
        {
            get => _surName;
            set => SetProperty(ref _surName, value);
        }

        string _firstName;
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        string _attendeeType;
        public string AttendeeType
        {
            get => _attendeeType;
            set => SetProperty(ref _attendeeType, value);
        }

        string _organisation;
        public string Organisation
        {
            get => _organisation;
            set => SetProperty(ref _organisation, value);
        }

        string _table;
        public string Table
        {
            get => _table;
            set => SetProperty(ref _table, value);
        }

        string _seatNumber;
        public string SeatNumber
        {
            get => _seatNumber;
            set => SetProperty(ref _seatNumber, value);
        }

        
    }
}
