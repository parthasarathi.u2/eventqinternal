using System;
using System.Threading.Tasks;
using EventQMobileApp;
using Prism.Navigation;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class LaunchPageViewModel : BasePageViewModel
    {
        public LaunchPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            LogUtility.Log($"Inside Constructor LaunchPageViewModel with FirstRun - {Settings.FirstRun}");

            Task.Run(async () =>
            {
                try
                {
                    LogUtility.Init(AppServiceLocator.Resolve<ILogService>());

                    await Task.Run(() => { })
                         .ContinueWith(t =>
                         {
                             ContinueNavigation();
                         });
                }
                catch (Exception ex)
                {
                    await AppInsights.ReportAsync(ex);
                }
            });

            LogUtility.Log("After constructor LaunchPageViewModel");
        }

        private void ContinueNavigation()
        {

            Device.BeginInvokeOnMainThread(async () =>
            {
                if (Settings.FirstRun)                
                    await NavigationService.NavigateAsync(new Uri($"/{nameof(LoginPage)}"));                
                else                
                    await NavigationService.NavigateAsync(new Uri($"/{nameof(MainPage)}"));                
            });
        }
    }
}
