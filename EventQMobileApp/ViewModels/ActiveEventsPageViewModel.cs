﻿using EventQMobileApp.Services.Repository;
using Prism.Commands;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class ActiveEventsPageViewModel : BasePageViewModel
    {

        #region Private Variables
        /// ------------------------------------------------------------------------------------------------        

        private readonly INavigationService _navigationService;
        private readonly IUserRepository _userRepository;
        private readonly IDataService _dataService;

        #endregion
        ///----------------------------------------------------------------------------------------------

        #region public Variables
        /// ------------------------------------------------------------------------------------------------        

        ObservableCollection<string> _listItemSource;
        public ObservableCollection<string> LstItemSource
        {
            get => _listItemSource;
            set => SetProperty(ref _listItemSource, value);
        }

        string _keyword;
        public string Keyword
        {
            get => _keyword;
            set => SetProperty(ref _keyword, value);
        }

        bool _isVisible;
        public bool IsVisible
        {
            get => _isVisible;
            set => SetProperty(ref _isVisible, value);
        }

        bool _isLoading;
        public bool IsLoading
        {
            get => _isLoading;
            set => SetProperty(ref _isLoading, value);
        }

        bool _isVisibleSyncing;
        public bool IsVisibleSyncing
        {
            get => _isVisibleSyncing;
            set => SetProperty(ref _isVisibleSyncing, value);
        }

        bool _isSyncing;
        public bool IsSyncing
        {
            get => _isSyncing;
            set => SetProperty(ref _isSyncing, value);
        }

        private bool _isAttendeeListLoading;
        public bool IsAttendeeListLoading
        {
            get => _isAttendeeListLoading;
            set => SetProperty(ref _isAttendeeListLoading, value);
        }

        Event _selectedItem;
        public Event SelectedItem
        {
            get => _selectedItem;
            set
            {
                PreviousItem = _selectedItem;
                SetProperty(ref _selectedItem, value);
                if(value != null)
                {
                    value.IsSelected = true;
                    if(PreviousItem != null) PreviousItem.IsSelected = false;
                }
            }
        }

        Event _previousItem;
        public Event PreviousItem
        {
            get => _previousItem;
            set
            {
                _previousItem = value;
            }
        }

        List<Event> _eventModelList;
        public List<Event> EventModelList
        {
            get
            {
                if (_eventModelList == null)
                    _eventModelList = new List<Event>();
                return _eventModelList;
            }
            set => SetProperty(ref _eventModelList, value);
        }

        ObservableCollection<Event> _eventList;
        public ObservableCollection<Event> EventsList
        {
            get
            {
                if (_eventList == null)
                    _eventList = new ObservableCollection<Event>();
                return _eventList;
            }
            set => SetProperty(ref _eventList, value);
        }

        DelegateCommand _searchKeywordChangedCommand;
        public DelegateCommand SearchKeywordChangedCommand => _searchKeywordChangedCommand ??
            (_searchKeywordChangedCommand = new DelegateCommand(async () =>
            {
                await Task.Run(async () =>
                {
                    try
                    {
                        /**/
                        EventsList.Clear();
                        if (!string.IsNullOrEmpty(Keyword))
                        {
                            var eventsList = EventModelList.Where(x => x.EventName.ContainsIgnoreCase(Keyword)).ToList();

                            EventsList = new ObservableCollection<Event>(eventsList);
                        }
                        else
                        {
                            EventsList = new ObservableCollection<Event>(EventModelList);
                        }
                    }
                    catch (Exception ex)
                    {
                        await AppInsights.ReportAsync(ex, AppInsights.Severity.Error);
                    }
                });
            }));

        DelegateCommand _signOutCommand;
        public DelegateCommand SignOutCommand => _signOutCommand ?? (_signOutCommand = new DelegateCommand(async () =>
        {
            await _navigationService.NavigateAsync(nameof(LoginPage));
        }));

        DelegateCommand<Event> _listItemTappedCommand;
        public DelegateCommand<Event> ListItemTappedCommand => _listItemTappedCommand ??
            (_listItemTappedCommand = new DelegateCommand<Event>(async (_event) =>
        {
            await NavigateToMainPage(_event);
        }));

        DelegateCommand<Event> _syncMenuCommand;
        public DelegateCommand<Event> SyncMenuCommand => _syncMenuCommand ?? (_syncMenuCommand = new DelegateCommand<Event>(async (_event) =>
        {
            await NavigateToMainPage(_event);
        }));

        DelegateCommand _syncAllCommand;
        public DelegateCommand SyncAllCommand => _syncAllCommand ?? (_syncAllCommand = new DelegateCommand(async () =>
        {
            await LoadEvents();
        }));

        #endregion
        ///----------------------------------------------------------------------------------------------

        #region Constructor
        /// ------------------------------------------------------------------------------------------------        

        public ActiveEventsPageViewModel(
            INavigationService navigationService,
            IUserRepository userRepository,
            IDataService dataService) : base(navigationService)
        {
            _navigationService = navigationService;
            _userRepository = userRepository;
            _dataService = dataService;

            IsVisibleSyncing = false;
            IsVisible = false;
            IsAttendeeListLoading = false;
            LoadEvents();

        }

        #endregion
        ///----------------------------------------------------------------------------------------------

        #region Private Functions 
        /// ------------------------------------------------------------------------------------------------        

        private async Task LoadEvents()
        {
            int page = 1;
            int recordCount = 5;
            IsLoading = true;
            IsSyncing = true;
            EventsList.Clear();
            EventModelList.Clear();
            while (true)
            {
                var responseList = await _dataService.GetEvents(page, recordCount);

                if (responseList != null && responseList.Count > 0)
                {
                    EventModelList.AddRange(responseList);

                    foreach (var item in responseList)
                    {
                        EventsList.Add(item);
                    }
                }
                if (responseList.Count < recordCount)
                {
                    IsLoading = false;
                    break;
                }
                page++;
            }
            IsSyncing = false;
        }

        private async Task NavigateToMainPage(Event _event)
        {
            NavigationParameters paramter = new NavigationParameters();
            paramter.Add("CurrentEvent", _event);
            await _navigationService.NavigateAsync(nameof(MainPage), paramter);
        }
        #endregion
        ///----------------------------------------------------------------------------------------------
    }
}


