﻿using System;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class AttendeeBaseViewModel : BaseViewModel
    {

        public Guid AttendeeId { get; set; }

        string _firstName;
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }

        string _surName;
        public string SurName
        {
            get => _surName;
            set => SetProperty(ref _surName, value);
        }

        DateTime? _checkInTime;
        public DateTime? CheckInTime
        {
            get => _checkInTime;
            set => SetProperty(ref _checkInTime, value);
        }

        string _attendeeType;
        public string AttendeeType
        {
            get => _attendeeType;
            set => SetProperty(ref _attendeeType, value);
        }

        bool _isVip;
        public bool IsVip
        {
            get => _isVip;
            set => SetProperty(ref _isVip, value);
        }

        string _organisation;
        public string Organisation
        {
            get => _organisation;
            set => SetProperty(ref _organisation, value);
        }

        string _table;
        public string Table
        {
            get => _table;
            set => SetProperty(ref _table, value);
        }

        string _seatNumber;
        public string SeatNumber
        {
            get => _seatNumber;
            set => SetProperty(ref _seatNumber, value);
        }

        bool _isEntranceDone;
        public bool IsEntranceDone
        {
            get => _isEntranceDone;
            set => SetProperty(ref _isEntranceDone, value);
        }

        DelegateCommand<AttendeeBaseViewModel> _checkInCommand;
        public DelegateCommand<AttendeeBaseViewModel> CheckInCommand => _checkInCommand ??
            (_checkInCommand = new DelegateCommand<AttendeeBaseViewModel>((commandParameter) =>
        {
            if (commandParameter != null)
            {
                this.IsEntranceDone = !commandParameter.IsEntranceDone;
                CheckInTime = DateTime.Now;
            }
            MessagingCenter.Send<BaseViewModel, AttendeeBaseViewModel>(this, MessagingCenterValues.AttendeeSelected.GetDescription(), this);
        }));

        DelegateCommand _attendeeSelectedCommand;
        public DelegateCommand AttendeeSelectedCommand => _attendeeSelectedCommand ?? (_attendeeSelectedCommand = new DelegateCommand(() =>
        {
            MessagingCenter.Send<BaseViewModel, AttendeeBaseViewModel>(this, MessagingCenterValues.AttendeeSelected.GetDescription(), this);
        }));

    }
}
