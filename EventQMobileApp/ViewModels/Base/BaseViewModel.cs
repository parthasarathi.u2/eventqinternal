﻿using Prism.Mvvm;
using Xamarin.Essentials;

namespace EventQMobileApp
{
    public abstract class BaseViewModel : BindableBase
    {
        bool _isBusy;
        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }

        bool _allGood = true;
        public bool AllGood
        {
            get => _allGood;
            set => SetProperty(ref _allGood, value);
        }

        bool _isConnected = false;
        public bool IsConnected
        {
            get => _isConnected;
            set
            {
                SetProperty(ref _isConnected, value);
                AllGood = !value;
            }
        }

        string _connectionType;
        public string ConnectionType
        {
            get => _connectionType;
            set
            {
                SetProperty(ref _connectionType, value);
            }
        }

        public BaseViewModel()
        {
            Connectivity.ConnectivityChanged += OnConnectivityChanged;
        }

        public string NetworkAccess =>
            Connectivity.NetworkAccess.ToString();

        public string ConnectionProfiles
        {
            get
            {
                var profiles = string.Empty;
                foreach (var p in Connectivity.ConnectionProfiles)
                    profiles += "\n" + p.ToString();
                return profiles;
            }
        }

        ~BaseViewModel()
        {
            Connectivity.ConnectivityChanged -= OnConnectivityChanged;
        }

        void OnConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            OnPropertyChanged(nameof(ConnectionProfiles));
            OnPropertyChanged(nameof(NetworkAccess));
            ConnectionType = ConnectionProfiles;
        }
    }
}
