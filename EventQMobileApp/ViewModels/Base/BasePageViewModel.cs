﻿using Prism.Commands;
using Prism.Navigation;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Permissions.Abstractions;
using Plugin.Permissions;

namespace EventQMobileApp
{
    public class BasePageViewModel : BaseViewModel, INavigationAware
    {
        protected readonly INavigationService NavigationService;

        string _pageTitle;
        public string PageTitle
        {
            get => _pageTitle;
            set => SetProperty(ref _pageTitle, value);
        }

        private string _logo = "logo.png";
        public string Logo
        {
            get => _logo;
            set => SetProperty(ref _logo, value);
        }

        public DelegateCommand<string> NavigateCommand { get; private set; }

        public DelegateCommand<NavigationParameters> GoBackCommand { get; private set; }

        public BasePageViewModel(INavigationService navigationService)
        {
            NavigationService = navigationService;
            IsConnected = Plugin.Connectivity.CrossConnectivity.Current.IsConnected;

            NavigateCommand = new DelegateCommand<string>(async (page) =>
            {
                await NavigationService.NavigateAsync(page);
            });

            GoBackCommand = new DelegateCommand<NavigationParameters>((navParam) =>
            {
                bool modal = (Application.Current.MainPage.Navigation.ModalStack.Count > 0);
                var pageName = this.GetType().Name.Replace("ViewModel", "");
                if (navParam == null)
                {
                    navParam = new NavigationParameters();
                }
                navParam.Add(NavigationKeys.Page.GetDescription(), pageName);
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var result = await NavigationService.GoBackAsync(navParam, modal);
                    if (result.Success)
                    {

                    }
                });
            });
        }

        #region INavigationAware Implementation
        public virtual void OnNavigatedFrom(INavigationParameters parameters)
        {
            if (parameters.GetNavigationMode() == NavigationMode.Back)
                Plugin.Connectivity.CrossConnectivity.Current.ConnectivityChanged -= Current_ConnectivityChanged;
        }

        public virtual async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.GetNavigationMode() == NavigationMode.New)
            {
                Plugin.Connectivity.CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;
                //await Task.Run(async () => BusyQuote = await AppServiceLocator.Resolve<IGeneralService>()?.GetBusyQuote());
            }
        }

        public virtual void OnNavigatingTo(INavigationParameters parameters)
        {

        }
        #endregion

        protected async Task<PermissionStatus> CheckLocationPermissionStatus()
        {
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
            return status;
        }

        void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            IsConnected = e.IsConnected;
        }

        

    }
}
