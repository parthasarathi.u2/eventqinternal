﻿using System;
using Prism.Navigation;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class MobileAttendeeDetailsPageViewModel : BasePageViewModel
    {
        private AttendeeDetailsViewViewModel _attendeeDetailsViewModel;

        public AttendeeDetailsViewViewModel AttendeeDetailsViewViewModel
        {
            get
            {
                return _attendeeDetailsViewModel;
            }
        }

        public MobileAttendeeDetailsPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            _attendeeDetailsViewModel = new AttendeeDetailsViewViewModel(navigationService);
            RaisePropertyChanged(nameof(AttendeeDetailsView));
        }
    }
}
