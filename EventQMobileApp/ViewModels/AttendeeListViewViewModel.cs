﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Navigation;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class AttendeeListViewViewModel : BasePageViewModel, IDisposable
    {
        INavigationService _navigationService;
        IDataService _dataService;

        public List<Attendee> BaseAttendeesList { get; set; }

        ObservableCollection<AttendeeBaseViewModel> _listItemSource;
        public ObservableCollection<AttendeeBaseViewModel> ListItemSource
        {
            get => _listItemSource;
            set => SetProperty(ref _listItemSource, value);
        }

        List<AttendeeBaseViewModel> _attendeesList;
        public List<AttendeeBaseViewModel> AttendeesList
        {
            get => _attendeesList;
            set => SetProperty(ref _attendeesList, value);
        }

        Attendee _selectedItem;
        public Attendee SelectedItem
        {
            get { return _selectedItem; }

            set
            {
                _selectedItem = value;
                if (value != null)
                {
                    if (Device.Idiom == TargetIdiom.Phone)
                    {
                        NavigationService.NavigateAsync(nameof(MobileAttendeeDetailsPage));
                    }
                    MessagingCenter.Send<BasePageViewModel, Attendee>(this, MessagingCenterValues.AttendeeModel.GetDescription(), value);
                }
            }
        }

        List<string> _attendeeTypeList;
        public List<string> AttendeeTypeList
        {
            get
            {
                if (_attendeeTypeList == null)
                {
                    _attendeeTypeList = Enum.GetValues(typeof(AttendeesType)).Cast<AttendeesType>().Select(v => v.GetDescription()).ToList();
                }
                return _attendeeTypeList;
            }
        }

        string _selectedAttendeeType;
        public string SelectedAttendeeType
        {
            get => _selectedAttendeeType;
            set
            {
                SetProperty(ref _selectedAttendeeType, value);
                SearchResult(SelectedAttendeeType, SelectedFilterType, _searchKeyword);
            }
        }

        List<string> _filterTypeList;
        public List<string> FilterTypeList
        {
            get
            {
                if (_filterTypeList == null)
                {
                    _filterTypeList = Enum.GetValues(typeof(FiltersType)).Cast<FiltersType>().Select(v => v.GetDescription()).ToList();
                }
                return _filterTypeList;
            }
        }

        string _selectedFilterType;
        public string SelectedFilterType
        {
            get => _selectedFilterType;
            set
            {
                SetProperty(ref _selectedFilterType, value);
                SearchResult(SelectedAttendeeType, SelectedFilterType, _searchKeyword);
            }
        }

        string _searchKeyword;
        public string SearchKeyword
        {
            get => _searchKeyword;
            set
            {
                SetProperty(ref _searchKeyword, value);
                SearchResult(SelectedAttendeeType, SelectedFilterType, _searchKeyword);
            }
        }

        bool _isClearEnabled;
        public bool IsClearEnabled
        {
            get => _isClearEnabled;
            set => SetProperty(ref _isClearEnabled, value);
        }

        DelegateCommand _clearCommand;
        public DelegateCommand ClearCommand => _clearCommand ?? (_clearCommand = new DelegateCommand(() =>
        {
            SearchKeyword = string.Empty;
            SelectedAttendeeType = AttendeesType.All.GetDescription();
            SelectedFilterType = FiltersType.No_Filter.GetDescription();
            ListItemSource?.Clear();
            ListItemSource = new ObservableCollection<AttendeeBaseViewModel>(AttendeesList);
            IsClearEnabled = false;
        }));

        public AttendeeListViewViewModel(INavigationService navigationService,
            IDataService dataService,
            IAutoMapper autoMapper) : base(navigationService)
        {
            _navigationService = navigationService;
            _dataService = dataService;

            BaseAttendeesList = _dataService.GetAttendees().ToList();

            AttendeesList = new List<AttendeeBaseViewModel>();
            foreach (var baseAttendee in BaseAttendeesList)
            {
                var attendee = autoMapper.GetMapper().Map<AttendeeBaseViewModel>(baseAttendee);
                AttendeesList.Add(attendee);
            }

            ListItemSource = new ObservableCollection<AttendeeBaseViewModel>(AttendeesList);

            MessagingCenter.Subscribe<BaseViewModel, AttendeeBaseViewModel>(this, MessagingCenterValues.AttendeeSelected.GetDescription(),
                (sender, attendee) =>
                {
                    var baseAttendee = BaseAttendeesList.FirstOrDefault(x => x.AttendeeId == attendee.AttendeeId);
                    if (baseAttendee != null)
                    {
                        baseAttendee.IsEntranceDone = attendee.IsEntranceDone;
                        baseAttendee.CheckInTime = attendee.CheckInTime;
                        MessagingCenter.Send<BasePageViewModel, Attendee>(this, MessagingCenterValues.AttendeeModel.GetDescription(), baseAttendee);
                    }
                });

            MessagingCenter.Subscribe<AttendeeDetailsViewViewModel, AttendeeBaseViewModel>(this, MessagingCenterValues.Edit.GetDescription(),
                (sender, attendee) =>
                {
                    var editedItem = ListItemSource.FirstOrDefault(x => x.AttendeeId == attendee.AttendeeId);
                    editedItem = attendee;
                });

        }

        private void SearchResult(string attendeeType, string filterType, string keyword)
        {
            var queryList = AttendeesList.ToList();
            if (!string.IsNullOrEmpty(attendeeType) && attendeeType != AttendeesType.All.GetDescription())
            {
                queryList = queryList.Where(x => x.AttendeeType == attendeeType).ToList();
            }

            if (!string.IsNullOrEmpty(filterType))
            {
                FiltersType filter;
                if (Enum.TryParse<FiltersType>(filterType, out filter))
                {
                    switch (filter)
                    {
                        case FiltersType.Checked_In:
                            queryList = queryList.Where(x => x.IsEntranceDone == true).ToList();
                            break;
                        case FiltersType.Missing:
                            queryList = queryList.Where(x => x.IsEntranceDone == false).ToList();
                            break;
                        case FiltersType.VIPs:
                            queryList = queryList.Where(x => x.IsVip == true).ToList();
                            break;
                        case FiltersType.No_Filter:
                            break;
                    }
                }
            }
            if (!string.IsNullOrEmpty(keyword))
            {
                queryList = queryList.Where(x => x.FirstName.Contains(keyword) || x.SurName.Contains(keyword)).ToList();
            }

            ListItemSource = new ObservableCollection<AttendeeBaseViewModel>(queryList);
            IsClearEnabled = true;
        }

        ~AttendeeListViewViewModel()
        {
            this.Dispose();
            MessagingCenter.Unsubscribe<BaseViewModel, AttendeeBaseViewModel>(this, MessagingCenterValues.AttendeeSelected.GetDescription());
            MessagingCenter.Unsubscribe<AttendeeDetailsViewViewModel, AttendeeBaseViewModel>(this, MessagingCenterValues.Edit.GetDescription());
        }

        public void Dispose()
        {
            AttendeesList = null;
            ListItemSource = null;
            GC.Collect();
        }
    }
}
