﻿using System;
using Prism.Commands;
using Prism.Navigation;

namespace EventQMobileApp
{
    public class EventViewCellViewModel : BasePageViewModel
    {

        public int EventId { get; set; }

        string _eventName;
        public string EventName
        {
            get => _eventName;
            set => SetProperty(ref _eventName, value);
        }

        string _address;
        public string Address
        {
            get => _address;
            set => SetProperty(ref _address, value);
        }

        int _maxAttendees;
        public int MaxAttendees
        {
            get => _maxAttendees;
            set => SetProperty(ref _maxAttendees, value);
        }

        float _percentage;
        public float Percentage
        {
            get => _percentage;
            set => SetProperty(ref _percentage, value);
        }

        int _checkedInAddtedees;
        public int CheckedInAddtedees
        {
            get => _checkedInAddtedees;
            set
            {
                _checkedInAddtedees = value;
                if(MaxAttendees > 0)
                    Percentage = (float)CheckedInAddtedees / MaxAttendees ;
            }
        }

        DateTime _startDate;
        public DateTime StartDate
        {
            get => _startDate;
            set => SetProperty(ref _startDate, value);
        }

        DelegateCommand _menuItemTappedCommand;
        public DelegateCommand MenuItemTappedCommand => _menuItemTappedCommand ?? (_menuItemTappedCommand = new DelegateCommand(async () =>
        {
            
        }));

        DelegateCommand _itemTappedCommand;
        public DelegateCommand ItemTappedCommand => _itemTappedCommand ?? (_itemTappedCommand = new DelegateCommand(async () =>
        {
            NavigationParameters navParameter = new NavigationParameters();
            navParameter.Add(NavigationKeys.Item.GetDescription(), EventId);
            await NavigationService.NavigateAsync(nameof(MainPage),navParameter);
        }));

        public EventViewCellViewModel(INavigationService navigationService) : base(navigationService)
        {

        }

    }
}
