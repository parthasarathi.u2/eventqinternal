﻿using System;
using Prism.Commands;
using Prism.Navigation;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class HeaderViewViewModel : BasePageViewModel
    {
        string _eventTitle;
        public string EventTitle
        {
            get => _eventTitle;
            set => SetProperty(ref _eventTitle, value);
        }

        string _eventDetails;
        public string EventDetails
        {
            get => _eventDetails;
            set => SetProperty(ref _eventDetails, value);
        }

        string _checkInCount;
        public string CheckInCount
        {
            get => _checkInCount;
            set => SetProperty(ref _checkInCount, value);
        }

        string _percentage;
        public string Percentage
        {
            get => _percentage;
            set => SetProperty(ref _percentage, value);
        }

        DelegateCommand _addAttendeeCommand;
        public DelegateCommand AddAttendeeCommand => _addAttendeeCommand ?? (_addAttendeeCommand = new DelegateCommand(async () =>
        {
            await PopupNavigation.Instance.PushAsync(new NewAttendeePopupView());
        }));

        DelegateCommand _syncTapCommand;
        public DelegateCommand SyncTapCommand => _syncTapCommand ?? (_syncTapCommand = new DelegateCommand(() =>
        {

        }));

        DelegateCommand _homeTapCommand;
        public DelegateCommand HomeTapCommand => _homeTapCommand ?? (_homeTapCommand = new DelegateCommand(async() =>
        {
            if (GoBackCommand.CanExecute(null))
            {
                GoBackCommand.Execute(null);
            }
        }));

        public HeaderViewViewModel(INavigationService navigationService) : base(navigationService)
        {
            ConnectionType = ConnectionTypes.Wifi.GetDescription();

            MessagingCenter.Subscribe<MainPageViewModel, Event>(this, MessagingCenterValues.CurrentEvent.GetDescription(),
                (sender, _event) =>
                {
                    EventTitle = _event.EventName;
                    EventDetails = _event.VenueName;
                    CheckInCount = string.Format("Checked-In({0} / {1})", _event.CheckedInCount, _event.RegisteredCount);
                    Percentage = string.Format(" {0}%", _event.Percentage);
                });
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
        }
    }
}
