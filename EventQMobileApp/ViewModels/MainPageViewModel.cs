﻿using System;
using Prism.Navigation;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class MainPageViewModel : BasePageViewModel
    {

        Event _selectedEvent;
        public Event SelectedEvent
        {
            get => _selectedEvent;
            set
            {
                SetProperty(ref _selectedEvent, value);
            }
        }

        private Lazy<AttendeeDetailsViewViewModel> _attendeeDetailsViewModel;

        public AttendeeDetailsViewViewModel AttendeeDetailsViewViewModel
        {
            get
            {
                return _attendeeDetailsViewModel?.Value;
            }
        }

        private Lazy<AttendeeListViewViewModel> _attendeeListViewModel;

        public AttendeeListViewViewModel AttendeeListViewViewModel
        {
            get
            {
                return _attendeeListViewModel?.Value;
            }
        }

        private Lazy<HeaderViewViewModel> _headerViewViewModel;

        public HeaderViewViewModel HeaderViewViewModel
        {
            get
            {
                return _headerViewViewModel?.Value;
            }
        }

        public MainPageViewModel(INavigationService navigationService,
            IDataService dataService,
            IAutoMapper autoMapper) : base(navigationService)
        {
            IsBusy = false;
            _attendeeDetailsViewModel = new Lazy<AttendeeDetailsViewViewModel>(() => new AttendeeDetailsViewViewModel(NavigationService));
            RaisePropertyChanged(nameof(AttendeeDetailsView));

            _attendeeListViewModel = new Lazy<AttendeeListViewViewModel>(() => new AttendeeListViewViewModel(navigationService, dataService, autoMapper));
            RaisePropertyChanged(nameof(AttendeeListView));

            _headerViewViewModel = new Lazy<HeaderViewViewModel>(() => new HeaderViewViewModel(navigationService));
            RaisePropertyChanged(nameof(HeaderView));

        }

        public override void OnNavigatedFrom(INavigationParameters parameters)
        {
            base.OnNavigatedFrom(parameters);
        }

        public override void OnNavigatingTo(INavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);

            SelectedEvent = parameters.GetValue<Event>("CurrentEvent");
            MessagingCenter.Send(this, MessagingCenterValues.CurrentEvent.GetDescription(), SelectedEvent);
        }
    }
}
