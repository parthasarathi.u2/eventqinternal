using System;
using EventQMobileApp.Services.Repository;
using Prism.Commands;
using Prism.Navigation;

namespace EventQMobileApp
{
    public class LoginPageViewModel : BasePageViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IUserRepository _userRepository;
        private readonly IDataService _dataService;

        string _username;
        public string Username
        {
            get => _username;
            set => SetProperty(ref _username, value);
        }

        string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        DelegateCommand _loginCommand;
        public DelegateCommand LoginCommand => _loginCommand ?? (_loginCommand = new DelegateCommand(async () =>
        {
            //TODO: Validation has to be done here.
            //if (await _dataService.SignInWithEmailPassword(Username.ToLower(), Password))
            //{
                await _navigationService.NavigateAsync(nameof(MainPage));
            //}
        }));

        public LoginPageViewModel(
            INavigationService navigationService,
            IUserRepository userRepository,
            IDataService dataService) : base(navigationService)
        {
            _navigationService = navigationService;
            _userRepository = userRepository;
            _dataService = dataService;
        }
    }
}
