using System.Collections.Generic;
using System.Threading.Tasks;namespace EventQMobileApp{    public interface IDataService    {

        #region UserService
        Task<StandardResponse<User>> GetUser(User user);

        IEnumerable<Attendee> GetAttendees();        Task<List<Event>> GetEvents(int? page, int? count = 30);
        #endregion

        #region Verification
        Task<bool> SignInWithEmailPassword(string emailAddress, string password);
        #endregion    }}