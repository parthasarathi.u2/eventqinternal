﻿using System.Threading.Tasks;

namespace EventQMobileApp
{
    public interface IVerificationService
    {

        //string VerificationId { get; set; }

        //Task<StandardResponse<string>> SendVerificationCode(string phoneNumber);

        //Task<StandardResponse<string>> VerifyPhoneNumber(string verificationId, string verificationCode);

        //Task<StandardResponse<string>> CreateAccountWithEmailPassword(string emailAddress, string password);

        Task<StandardResponse<string>> SignInWithEmailPassword(string emailAddress, string password);

        Task RefreshSession();

        //Task ForgotPassword(string emailAddress);
    }
}
