﻿using System;
using System.Threading.Tasks;

namespace EventQMobileApp
{
    public class VerificationService : BaseService, IVerificationService
    {
        public VerificationService(string baseUrl):base( baseUrl)
        {
        }

        public Task RefreshSession()
        {

        }

        public async Task<StandardResponse<string>> SignInWithEmailPassword(string emailAddress, string password)
        {

        }
    }
}
