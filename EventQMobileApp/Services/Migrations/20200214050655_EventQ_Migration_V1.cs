﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EventQMobileApp.Services.Repository
{
    public partial class EventQ_Migration_V1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AttendeeTable",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CheckInTime = table.Column<DateTime>(nullable: true),
                    SurName = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    AttendeeType = table.Column<string>(nullable: true),
                    Organisation = table.Column<string>(nullable: true),
                    AttendeeLabel = table.Column<string>(nullable: true),
                    Table = table.Column<string>(nullable: true),
                    SeatNumber = table.Column<string>(nullable: true),
                    IsEntranceDone = table.Column<bool>(nullable: false),
                    ContactType = table.Column<string>(nullable: true),
                    IsVip = table.Column<bool>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AttendeeTable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EventTable",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AbsentCount = table.Column<int>(nullable: false),
                    CheckedInCount = table.Column<int>(nullable: false),
                    EventId = table.Column<int>(nullable: false),
                    RegisteredCount = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    EventName = table.Column<string>(nullable: true),
                    EventRegistrationLink = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    TimezoneName = table.Column<string>(nullable: true),
                    VenueName = table.Column<string>(nullable: true),
                    StartTime = table.Column<TimeSpan>(nullable: true),
                    EndTime = table.Column<TimeSpan>(nullable: true),
                    Progress = table.Column<double>(nullable: false),
                    Percentage = table.Column<int>(nullable: false),
                    SyncState = table.Column<int>(nullable: false),
                    IsSelected = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastModified = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EventTable", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserTable",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Token = table.Column<string>(nullable: true),
                    IsAuthorized = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTable", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AttendeeTable");

            migrationBuilder.DropTable(
                name: "EventTable");

            migrationBuilder.DropTable(
                name: "UserTable");
        }
    }
}
