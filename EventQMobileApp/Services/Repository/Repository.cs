﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;

using Microsoft.EntityFrameworkCore;

namespace EventQMobileApp.Services.Repository
{
    public class Repository<T> : IRepository<T> where T : class, IEventQEntity
    {
        #region Public variable & properties
        //----------------------------------------------------------------------------------------------------

        public IDatabaseContext Context;

        //----------------------------------------------------------------------------------------------------
        #endregion

        #region Constructor
        //----------------------------------------------------------------------------------------------------		

        public Repository(IDatabaseContext context)
        {
            Context = context;
        }

        //----------------------------------------------------------------------------------------------------
        #endregion

        #region Public Methods
        //----------------------------------------------------------------------------------------------------

        public IEnumerable<T> Get() =>
            Context.Set<T>().ToList();

        public IEnumerable<T> Get<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null)
        {
            var query = Context.Set<T>().AsQueryable();

            if (predicate != null)
                query = query.Where(predicate);

            if (orderBy != null)
                query = query.OrderBy(orderBy);

            return query.ToList();
        }

        public T Get<TKey>(TKey id) =>
            Context.Set<T>().Find(id);

        public T Get(Expression<Func<T, bool>> predicate) =>
            Context.Set<T>().Find(predicate);

        public void Update(T entity) =>
            Context.Set<T>().Update(entity);

        public void Delete(T entity) =>
            Context.Set<T>().Remove(entity);

        public void Save() => Context.SaveChanges();

        public void Add(T entity)
        {
            if (entity.Id == null) entity.Id = Guid.NewGuid();

            Context.Set<T>().Add(entity);
            
        }

        public void AddRange(List<T> entity)
        {
            Context.Set<T>().AddRange(entity);
        }

        public T FindById(Guid id)
        {
            return Context.Set<T>().Find(id);
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate = null)
        {
            return
                Context.Set<T>()
                    .Where(e => e.IsDeleted == false)
                    .Where(predicate)
                    .ToList();
        }

        public T Single(Expression<Func<T, bool>> predicate = null)
        {
            return
                Context.Set<T>()
                    .Where(e => e.IsDeleted == false)
                    .Where(predicate)
                    .FirstOrDefault();
        }

        public bool Exists(Expression<Func<T, bool>> predicate = null)
        {
            return
                Context.Set<T>()
                    .Where(e => e.IsDeleted == false)
                    .Where(predicate)
                    .Any();
        }

        public void DetachEntity(T entity)
        {
            Context.Entry(entity).State = EntityState.Detached;
        }

        //----------------------------------------------------------------------------------------------------
        #endregion
    }
}
