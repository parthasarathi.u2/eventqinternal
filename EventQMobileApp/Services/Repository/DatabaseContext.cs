﻿using System;
using Microsoft.EntityFrameworkCore;
using Xamarin.Forms;

namespace EventQMobileApp.Services.Repository
{
    public sealed class DatabaseContext : DbContext, IDatabaseContext
    {
        #region Public variable & properties
        //----------------------------------------------------------------------------------------------------

        public DbSet<User> UserTable { get; set; }
        public DbSet<Event> EventTable { get; set; }
        public DbSet<Attendee> AttendeeTable { get; set; }

        //----------------------------------------------------------------------------------------------------
        #endregion

        #region Constructor
        //----------------------------------------------------------------------------------------------------		

        public DatabaseContext()
        {
            try
            {
                Database.Migrate();
            }
            catch (Exception ex)
            {
                LogUtility.Log(ex,VerbosityLevel.Error);
            }
        }

        //----------------------------------------------------------------------------------------------------
        #endregion

        #region Public Methods
        //----------------------------------------------------------------------------------------------------

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            try
            {
                optionsBuilder.UseSqlite($"Filename={DependencyService.Get<IDatabase>().GetSqlPath()}");
            }
            catch (Exception ex)
            {
                LogUtility.Log(ex, VerbosityLevel.Error);
            }
        }

        //----------------------------------------------------------------------------------------------------
        #endregion
    }
}
