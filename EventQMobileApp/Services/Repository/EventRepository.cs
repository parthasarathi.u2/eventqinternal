﻿using System;
namespace EventQMobileApp.Services.Repository
{
    public class EventRepository : Repository<Event> , IEventRepository
    {
        private readonly IDatabaseContext _dbContext;
        public EventRepository(IDatabaseContext context) : base(context)
        {
            _dbContext = context;
        }
    }
}
