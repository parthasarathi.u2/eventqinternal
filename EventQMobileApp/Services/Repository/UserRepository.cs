﻿using System;
using System.Linq;

namespace EventQMobileApp.Services.Repository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly IDatabaseContext _dbContext;
        public UserRepository(IDatabaseContext context) : base(context)
        {
            _dbContext = context;
        }

        public User GetSingleRecord()
        {
            var user = _dbContext.Set<User>().ToList();
            return null;
        }

        public bool IsUserExists(Guid id)
        {
            return Exists(x => x.Id == id);
        }
    }
}
