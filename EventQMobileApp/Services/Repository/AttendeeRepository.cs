﻿using System;

namespace EventQMobileApp.Services.Repository
{
    public class AttendeeRepository : Repository<Attendee>, IAttendeeRepository
    {
        private readonly IDatabaseContext _dbContext;

        public AttendeeRepository(IDatabaseContext context) : base(context)
        {
            _dbContext = context;
        }

        public bool CheckInAttendee(int Id)
        {
            return true;
        }
    }
}
