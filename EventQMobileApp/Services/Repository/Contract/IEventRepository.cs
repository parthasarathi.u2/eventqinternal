﻿namespace EventQMobileApp.Services.Repository
{
    public interface IEventRepository : IRepository<Event>
    {
    }
}