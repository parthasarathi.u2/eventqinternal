﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EventQMobileApp.Services.Repository
{
    public interface IRepository<T> where T : class, IEventQEntity
    {

        IEnumerable<T> Get();

        T Get<TKey>(TKey key);

        IEnumerable<T> Get<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null);

        T Get(Expression<Func<T, bool>> predicate);

        void Update(T entity);

        void Delete(T entity);

        void Save();

        void Add(T entity);

        void AddRange(List<T> entity);

        T FindById(Guid id);

        IEnumerable<T> Find(Expression<Func<T, bool>> predicate = null);

        T Single(Expression<Func<T, bool>> predicate = null);

        bool Exists(Expression<Func<T, bool>> predicate = null);

        void DetachEntity(T entity);

    }
}
