﻿using System;

namespace EventQMobileApp.Services.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        User GetSingleRecord();

        bool IsUserExists(Guid id);
    }
}
