﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace EventQMobileApp.Services.Repository
{
    public interface IDatabaseContext
    {
        #region properties
        //----------------------------------------------------------------------------------------------------
        
        DbSet<User> UserTable { get; set; }
        DbSet<Attendee> AttendeeTable { get; set; }

        //----------------------------------------------------------------------------------------------------
        #endregion

        #region Methods
        //----------------------------------------------------------------------------------------------------

        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();

        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        //----------------------------------------------------------------------------------------------------
        #endregion
    }
}
