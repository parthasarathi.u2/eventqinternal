﻿using System;

namespace EventQMobileApp
{
    public abstract class BaseLogService : ILogService
    {
        public virtual void Log(string message, VerbosityLevel verbosityLevel = VerbosityLevel.Debug,
            LogCategory logCategory = LogCategory.General)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine($"{DateTime.Now.ToFormattedDateTime(DateTimeFormat.DateTime) : verbosityLevel.ToString().ToUpper()}: {message}");
            }
            catch (Exception ex)
            {
                // Don't ever crash because of a logging issue
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }

        public virtual void Log(Exception ex, VerbosityLevel verbosityLevel = VerbosityLevel.Error,
            LogCategory logCategory = LogCategory.General)
        {
            Log(ex.ToString(), verbosityLevel, logCategory);
        }
    }
}
