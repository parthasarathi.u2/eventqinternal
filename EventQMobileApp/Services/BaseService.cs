﻿namespace EventQMobileApp
{
    public class BaseService
    {
        protected EventQRestClient _restClient;

        protected StandardResponse<T> ErrorResponse<T>()
        {
            var stdResponse = new StandardResponse<T> { Success = false, ErrorMessage = AppConfig.STANDARD_ERROR_MESSAGE };
            return stdResponse;
        }

        public BaseService(string baseUrl)
        {
            _restClient = new EventQRestClient(baseUrl);
        }
    }
}
