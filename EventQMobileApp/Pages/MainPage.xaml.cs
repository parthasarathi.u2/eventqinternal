﻿using System;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public partial class MainPage : BasePage
    {
        /// ------------------------------------------------------------------------------------------------        
        #region Private Variables and Properties

        private double _width;
        private double _height;
        #endregion
        /// ------------------------------------------------------------------------------------------------
        
        public MainPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false); 
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            try
            {
                base.OnSizeAllocated(width, height);
                if (width.CompareTo(_width) < 0 || height.CompareTo(_height) < 0)
                {
                    _width = width;
                    _height = height;
                    if (width < height)
                    {
                        GlOuter.ColumnDefinitions.Clear();
                        GlOuter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(25, GridUnitType.Star) });
                        GlOuter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50, GridUnitType.Star) });
                        GlOuter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(25, GridUnitType.Star) });

                        GlOuter.RowDefinitions.Clear();
                        GlOuter.RowDefinitions.Add(new RowDefinition { Height = new GridLength(30, GridUnitType.Star) });
                        GlOuter.RowDefinitions.Add(new RowDefinition { Height = new GridLength(70, GridUnitType.Star) });

                    }
                    else
                    {
                        GlOuter.ColumnDefinitions.Clear();
                        GlOuter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(34, GridUnitType.Star) });
                        GlOuter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(32, GridUnitType.Star) });
                        GlOuter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(34, GridUnitType.Star) });

                        GlOuter.RowDefinitions.Clear();
                        GlOuter.RowDefinitions.Add(new RowDefinition { Height = new GridLength(25, GridUnitType.Star) });
                        GlOuter.RowDefinitions.Add(new RowDefinition { Height = new GridLength(75, GridUnitType.Star) });

                    }

                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
    }
}
