﻿using Xamarin.Forms;

namespace EventQMobileApp
{
    public partial class LaunchPage : BasePage
    {
        public LaunchPage()
        {
            InitializeComponent();
            BackgroundColor = Color.Black;
        }
    }
}
