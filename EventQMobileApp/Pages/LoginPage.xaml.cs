﻿namespace EventQMobileApp
{
    public partial class LoginPage : BasePage
    {
        public LoginPage()
        {
            InitializeComponent();
            NavigationPageExt.SetHasNavigationBar(this, false);
        }
    }
}
