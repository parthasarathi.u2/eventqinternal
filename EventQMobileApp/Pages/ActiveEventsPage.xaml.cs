﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventQMobileApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace EventQMobileApp
{
   [XamlCompilation (XamlCompilationOptions.Compile)]
    public partial class ActiveEventsPage : BasePage
    {
        private double width;
        private double height;
        public ActiveEventsPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            try
            {

                base.OnSizeAllocated(width, height);
                if (width != this.width || height != this.height)
                {
                    this.width = width;
                    this.height = height;

                    if (width < height)
                    {
                        if (MainGrid != null)
                        {
                            MainGrid.RowDefinitions.Clear();
                            MainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(120, GridUnitType.Absolute) });
                            MainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.1, GridUnitType.Star) });
                            MainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(91.9, GridUnitType.Star) });


                        }
                        if (SubGrid != null)
                        {
                            SubGrid.ColumnDefinitions.Clear();
                            SubGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0, GridUnitType.Star) });
                            SubGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100, GridUnitType.Star) });
                            SubGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(0, GridUnitType.Star) });

                            SubGrid.RowDefinitions.Clear();
                            SubGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.5, GridUnitType.Absolute) });
                            SubGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(5, GridUnitType.Absolute) });
                            SubGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(99.5, GridUnitType.Star) });
                        }
                        liststack.Orientation = StackOrientation.Vertical;
                    }
                    else
                    {
                        if (MainGrid != null)
                        {
                            MainGrid.RowDefinitions.Clear();
                            MainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(120, GridUnitType.Absolute) });
                            MainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.1, GridUnitType.Star) });
                            MainGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(91.9, GridUnitType.Star) });

                        }
                        if (SubGrid != null)
                        {
                            SubGrid.ColumnDefinitions.Clear();
                            SubGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(20, GridUnitType.Star) });
                            SubGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60, GridUnitType.Star) });
                            SubGrid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(20, GridUnitType.Star) });

                            SubGrid.RowDefinitions.Clear();
                            SubGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(6, GridUnitType.Absolute) });
                            SubGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.9, GridUnitType.Star) });
                            SubGrid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(93.1, GridUnitType.Star) });
                        }
                        liststack.Orientation = StackOrientation.Horizontal;

                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }

        private void OnTextChanged(object sender,EventArgs args)
        {
            var viewModel = ((ActiveEventsPageViewModel)BindingContext);
            if (viewModel.SearchKeywordChangedCommand.CanExecute())
            {
                viewModel.SearchKeywordChangedCommand.Execute();
            }
        }
    }
}