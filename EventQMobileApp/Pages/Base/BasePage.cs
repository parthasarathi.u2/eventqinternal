﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace EventQMobileApp
{
    public abstract class BasePage : ContentPage
    {
        public BasePage()
        {
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            BackgroundColor = Color.White;
        }

        public BasePageViewModel PageViewModel { get; private set; }

        public T PageViewModelAs<T>()
            where T : BasePageViewModel
        {
            if (PageViewModel is T vm)
            {
                return vm;
            }

            throw new InvalidCastException($"Expected page model to be '{typeof(T)}' but was '{PageViewModel.GetType()}'.");
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (BindingContext is BasePageViewModel pageModel)
            {
                PageViewModel = pageModel;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            LogUtility.Log($"{GetType()} OnAppearing; ViewModel is '{PageViewModel?.GetType()}';", VerbosityLevel.Info);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            LogUtility.Log($"{GetType()} OnDisappearing; ViewModel is '{PageViewModel?.GetType()}';", VerbosityLevel.Info);
        }
    }
}
