﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class BooleanToColorConverter : IValueConverter
    {
        public Color SuccessColor { get; set; }
        public Color FailureColor { get; set; }

        #region IValueConverter implementation
        public object Convert(object success, Type targetType, object parameter, CultureInfo culture)
        {
            if (success is bool)
            {
                if ((bool)success)
                    return SuccessColor;
            }
            return FailureColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Color && (Color)value == SuccessColor)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}
