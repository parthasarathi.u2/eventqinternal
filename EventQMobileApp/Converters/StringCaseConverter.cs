﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class StringCaseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            string param = System.Convert.ToString(parameter) ?? "UPPERCASE";

            switch (param.ToUpper())
            {
                case "UPPERCASE":
                    return ((string)value).ToUpper();
                case "LOWERCASE":
                    return ((string)value).ToLower();
                default:
                    return ((string)value);
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
