﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class StatusColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Status && value != null)
            {
                Status status = (Status)value;
                Color color;
                
                switch (status)
                {
                    case Status.Checked_In:
                        color = Color.LimeGreen;
                        break;
                    case Status.Time_Not_Available:
                        color = Color.Yellow;
                        break;
                    case Status.Not_Checked_In_Yet:
                        color = Color.Red;
                        break;
                    default:
                        color = Color.Black;
                        break;
                }
                return color;
            }
            return Color.Default;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
