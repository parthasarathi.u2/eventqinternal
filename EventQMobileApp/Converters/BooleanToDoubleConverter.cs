﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class BooleanToDoubleConverter : IValueConverter
    {
        public double SuccessValue { get; set; }
        public double FailureValue { get; set; }

        #region IValueConverter implementation
        public object Convert(object success, Type targetType, object parameter, CultureInfo culture)
        {
            if (success is bool)
            {
                if ((bool)success)
                    return SuccessValue;
            }
            return FailureValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double && ((double)value).CompareTo(SuccessValue) == 0)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}
