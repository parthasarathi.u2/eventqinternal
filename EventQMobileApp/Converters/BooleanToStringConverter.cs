﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class BooleanToStringConverter : IValueConverter
    {
        public string SuccessValue { get; set; }
        public string FailureValue { get; set; }

        #region IValueConverter implementation
        public object Convert(object success, Type targetType, object parameter, CultureInfo culture)
        {
            if (success is bool)
            {
                if ((bool)success)
                    return SuccessValue;
            }
            return FailureValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string && (string)value == SuccessValue)
            {
                return true;
            }
            return false;
        }
        #endregion

    }
}
