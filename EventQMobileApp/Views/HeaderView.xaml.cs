﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public partial class HeaderView : ContentView
    {
        /// ------------------------------------------------------------------------------------------------        
        #region Private Variables and Properties

        private double _width;
        private double _height;
        #endregion
        /// ------------------------------------------------------------------------------------------------

        public HeaderView()
        {
            InitializeComponent();
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            try
            {
                base.OnSizeAllocated(width, height);
                //TODO: Resize the UI based on Orientation
                //if (width.CompareTo(_width) > 0 || height.CompareTo(_height) > 0)
                //{
                //    if (_height < height && _height.CompareTo(0) > 0)
                //    {
                //        Add_Frame.Margin = new Thickness(10);
                //    }
                //    else
                //    {
                //        Add_Frame.Margin = new Thickness(0);

                //    }
                //    _width = width;
                //    _height = height;
                //}
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }

        }
    }
}
