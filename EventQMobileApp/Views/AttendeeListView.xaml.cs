﻿using Xamarin.Forms;

namespace EventQMobileApp
{
    public partial class AttendeeListView : ContentView
    {
        /// ------------------------------------------------------------------------------------------------        
        #region Private Variables and Properties

        private double _width;
        private double _height;
        #endregion
        /// ------------------------------------------------------------------------------------------------
        
        public AttendeeListView()
        {
            InitializeComponent();
        }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if (width.CompareTo(_width) > 0 || height.CompareTo(_height) > 0)
            {
                _width = width;
                _height = height;
                if (width < height)
                {
                    //LandScapeView

                    //GlMain
                    if (GlMain != null)
                    {
                        GlMain.RowDefinitions.Clear();
                        GlMain.RowDefinitions.Add(new RowDefinition { Height = new GridLength(5, GridUnitType.Star) });
                        GlMain.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.1, GridUnitType.Star) });
                        GlMain.RowDefinitions.Add(new RowDefinition { Height = new GridLength(94.9, GridUnitType.Star) });
                        AttendeeSearchBar.CornerRadius = 40;
                        AttendeeSearchBar.BorderWidth = 9;
                    }
                    //GlHeader
                    if (GlHeader != null)
                    {
                        GlHeader.ColumnDefinitions.Clear();
                        GlHeader.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(21, GridUnitType.Star) });
                        GlHeader.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(25, GridUnitType.Star) });
                        GlHeader.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(56, GridUnitType.Star) });
                    }
                }
                else
                {
                    //PortraitView

                    //GlMain
                    if (GlMain != null)
                    {
                        GlMain.RowDefinitions.Clear();
                        GlMain.RowDefinitions.Add(new RowDefinition { Height = new GridLength(8, GridUnitType.Star) });
                        GlMain.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.1, GridUnitType.Star) });
                        GlMain.RowDefinitions.Add(new RowDefinition { Height = new GridLength(91.9, GridUnitType.Star) });
                        AttendeeSearchBar.CornerRadius = 30;
                        AttendeeSearchBar.BorderWidth = 14;
                    }
                    //GlHeader
                    if (GlHeader != null)
                    {
                        GlHeader.ColumnDefinitions.Clear();
                        GlHeader.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(24, GridUnitType.Star) });
                        GlHeader.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(25, GridUnitType.Star) });
                        GlHeader.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(51, GridUnitType.Star) });
                    }
                }
            }
        }
    }
}
