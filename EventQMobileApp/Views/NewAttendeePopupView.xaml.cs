﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;

namespace EventQMobileApp
{
    public partial class NewAttendeePopupView : PopupPage
	{
        public NewAttendeePopupView()
        {
            InitializeComponent();
        }
        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (width > height)
            {
                parentFrame.HeightRequest = AppConfig.SCREEN_HEIGHT * 0.75;
                parentFrame.WidthRequest = AppConfig.SCREEN_WIDTH * 0.70;
            }
            else
            {
                parentFrame.HeightRequest = AppConfig.SCREEN_HEIGHT * 0.70;
                parentFrame.WidthRequest = AppConfig.SCREEN_WIDTH * 0.80;
            }
        }
        public async void OnCancel_Clicked(object sender,EventArgs args)
        {
            await PopupNavigation.Instance.PopAsync();
        }

    }
}
