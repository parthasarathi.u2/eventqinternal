﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class RestClientHelper
    {
        readonly string _baseUrl;

        public RestClientHelper(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        HttpClient CreateRestClient(List<KeyValuePair<string, string>> specificRequestHeadersList = null)
        {
            HttpClient httpClient = new HttpClient();

            httpClient.BaseAddress = new Uri(_baseUrl);

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            if (specificRequestHeadersList != null && specificRequestHeadersList.Any())
            {
                foreach (var kvp in specificRequestHeadersList)
                {
                    if (string.IsNullOrWhiteSpace(kvp.Key) || string.IsNullOrWhiteSpace(kvp.Value))
                        continue;

                    switch (kvp.Key)
                    {

                        case "jwt":
                        case "oauth2":
                            {
                                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", kvp.Value);
                            }
                            break;
                        default:
                            {
                                httpClient.DefaultRequestHeaders.Add(kvp.Key, kvp.Value);
                            }
                            break;
                    }
                }
            }
            return httpClient;
        }

        #region Get Call

        [InsightsLogger]
        public virtual async Task<T> GetHttpResponse<T>(string extraParam, List<KeyValuePair<string, string>> headerList = null)
        {
            try
            {
                using (var httpClient = CreateRestClient(headerList))
                {
                    var webApiUri = string.Format("{0}{1}", _baseUrl, extraParam);
                    LogUtility.Log($"GET - {extraParam} - Request " + webApiUri);
                    LogUtility.Log($"GET - {extraParam} - Header " + httpClient.DefaultRequestHeaders);
                    //Stopwatch.StartNew();
                    var response = await httpClient.GetAsync(webApiUri);
                    //LogUtility.Log(Stopwatch.GetTimestamp());
                    // this will handle the 400 exception
                    response.EnsureSuccessStatusCode();
                    var result = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
                    LogUtility.Log($"GET - {extraParam} - Result {result}");
#if RELEASE
                    await AppInsights.TrackAsync(string.Format("[Request] GET {0}", (_baseUrl + extraParam)));
#else
                    await AppInsights.TrackAsync(string.Format("[Request] {0} [Header] {1}", (_baseUrl + extraParam), httpClient.DefaultRequestHeaders));
#endif
                    return result;
                }
            }
            catch (WebException webEx)
            {
                CheckForSSLIssues(webEx);
                throw;
            }
            catch (Exception ex)
            {
                await AppInsights.ReportAsync(ex);
                throw;
            }
        }

        #endregion

        #region Post Call

        [InsightsLogger]
        public virtual async Task<T> PostHttpResponse<T>(string extraParam, string data, List<KeyValuePair<string, string>> headerList = null) where T : new()
        {
            try
            {
                using (var httpClient = CreateRestClient(headerList))
                {
                    LogUtility.Log("POST - Request " + _baseUrl + extraParam);
                    LogUtility.Log($"POST - {extraParam} - Header " + httpClient.DefaultRequestHeaders);
                    LogUtility.Log($"POST - {extraParam } - Body " + data);

                    HttpContent httpContent;
                    if (!extraParam.Equals(AppConfig.VALIDATION))
                    {
                        httpContent = new StringContent(data, Encoding.UTF8, "application/json");
                    }
                    else
                    {
                        httpContent = new FormUrlEncodedContent(headerList);
                    }

                    var response = await httpClient.PostAsync(_baseUrl + extraParam, httpContent);

                    response.EnsureSuccessStatusCode();
                    LogUtility.Log($"POST - {extraParam} - Response for {extraParam} is {response.Content.ReadAsStringAsync().Result}");
                    var result =  JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
#if RELEASE
                    await AppInsights.TrackAsync(string.Format("[Request] POST {0}", (_baseUrl + extraParam)));
#else
                    await AppInsights.TrackAsync(string.Format("[Request] {0} [Header] {1} [PostBody] {2}", (_baseUrl + extraParam), httpClient.DefaultRequestHeaders, data));
#endif
                    return result;
                }
            }
            catch (WebException webEx)
            {
                CheckForSSLIssues(webEx);
                throw;
            }
            catch (Exception ex)
            {
                LogUtility.Log("Rest Client - POST - Exception Occured " + ex.Message);
                await AppInsights.ReportAsync(ex);
                throw;
            }
        }

        #endregion

        #region Delete Call

        [InsightsLogger]
        public virtual async Task<T> DeleteHttpResponse<T>(string extraParam, string data, List<KeyValuePair<string, string>> headerList = null)
        {
            try
            {
                using (var httpClient = CreateRestClient(headerList))
                {

                    LogUtility.Log("DELETE - Request " + _baseUrl + extraParam + data);
                    var response = await httpClient.DeleteAsync(_baseUrl + extraParam + data);
                    var result = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                    LogUtility.Log($"DELETE - Result for {extraParam} is {result}");
#if RELEASE
                    await AppInsights.TrackAsync(string.Format("[Request] DEL {0}", (_baseUrl + extraParam)));
#else
                    await AppInsights.TrackAsync(string.Format("[Request] {0} [Header] {1}", (_baseUrl + extraParam), httpClient.DefaultRequestHeaders));
#endif
                    return result;
                }
            }
            catch (WebException webEx)
            {
                CheckForSSLIssues(webEx);
                throw;
            }
            catch (Exception ex)
            {
                LogUtility.Log("Rest Client - DELETE - Exception Occured " + ex.Message + " " + ex.StackTrace);
                await AppInsights.ReportAsync(ex);
                throw;
            }
        }

        #endregion

        #region Put Call

        [InsightsLogger]
        public virtual async Task<T> PutHttpResponse<T>(string extraParam, string data, List<KeyValuePair<string, string>> headerList = null) where T : new()
        {
            try
            {
                using (var httpClient = CreateRestClient(headerList))
                {
                    LogUtility.Log("PUT - Request " + _baseUrl + extraParam);
                    LogUtility.Log($"PUT - {extraParam} - Header " + httpClient.DefaultRequestHeaders);
                    LogUtility.Log($"PUT - {extraParam} - Body " + data);

                    var response = await httpClient.PutAsync(_baseUrl + extraParam, new StringContent(data, Encoding.UTF8, "application/json"));
                   
                    LogUtility.Log($"PUT - Result for {extraParam} is {response.Content.ReadAsStringAsync().Result}");
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var result = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);

#if RELEASE
                        await AppInsights.TrackAsync(string.Format("[Request] PUT {0} ", _baseUrl + extraParam));
#else
                        await AppInsights.TrackAsync(string.Format("[Request] {0} [Header] {1} [PutBody] {2}", _baseUrl + extraParam, httpClient.DefaultRequestHeaders, data));
#endif
                        return result;
                    }
                    else if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        string msg = response.Content.ReadAsStringAsync().Result.Trim();
                        LogUtility.Log("Auth Exception occured " + msg);
                    }
                    throw new Exception("Error occured. Try again.");
                }
            }
            catch (WebException webEx)
            {
                CheckForSSLIssues(webEx);
                throw;
            }
            catch (Exception ex)
            {
                await AppInsights.ReportAsync(ex);
                throw;
            }
        }

        #endregion


        private bool CheckForSSLIssues(WebException webEx)
        {
            if ((int)webEx.Status == 9)//Trust Failure -> Not available as Enum
            {
                MessagingCenter.Send<RestClientHelper>(this, "LockApp");
                Device.BeginInvokeOnMainThread(() =>
                {
                    Application.Current.MainPage.DisplayAlert("Security Alert", "We cannot establish a trusted connection at the moment. For security reasons we cannot proceed.", "OK");
                });
                return true;
            }
            return false;
        }
    }
}
