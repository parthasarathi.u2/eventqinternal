﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventQMobileApp
{
    public static class AppInsights
    {
        public enum Severity
        {
            Critical = 0,
            Error = 1,
            Warning = 2
        }

        public static void Identify(string name, Dictionary<string, string> userDetailsDictionary)
        {

        }

        public static async Task TrackAsync(string message)
        {

            var appInsightsService = AppServiceLocator.Resolve<IAppInsightsService>();
            if (appInsightsService != null)
            {
                await appInsightsService.TrackAsync(message);
            }
        }

        public static async Task ReportAsync(Exception ex, Severity severity = Severity.Critical)
        {
            var appInsightsService = AppServiceLocator.Resolve<IAppInsightsService>();
            if (appInsightsService != null)
            {
                await appInsightsService.ReportAsync(ex);
            }
        }
    }
}
