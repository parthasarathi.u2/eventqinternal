using System;
using System.Reflection;
using System.Threading.Tasks;
using EventQMobileApp;

[module: InsightsLogger]
namespace EventQMobileApp
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Constructor | AttributeTargets.Assembly | AttributeTargets.Module)]
    public class InsightsLoggerAttribute : Attribute, IMethodDecorator
    {
        private string _methodName;


        public void Init(object instance, MethodBase method, object[] args)
        {
            _methodName = method.DeclaringType.FullName + "." + method.Name;
        }


        public async Task OnEntry()
        {
            var message = string.Format("OnEntry: {0}", _methodName);
            await AppInsights.TrackAsync(message);
        }


        public async Task OnExit()
        {
            var message = string.Format("OnExit: {0}", _methodName);
            await AppInsights.TrackAsync(message);
        }


        public async Task OnException(Exception exception)
        {
            await AppInsights.ReportAsync(exception, AppInsights.Severity.Error);
        }
    }
}
