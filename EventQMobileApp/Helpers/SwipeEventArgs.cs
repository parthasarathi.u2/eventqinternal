﻿using System;
namespace EventQMobileApp
{
    public class SwippedItemEventArgs : EventArgs
    {

        public object Group { get; set; }

        public SwipeDirection Direction { get; set; }

        public object Item { get; set; }

        public int ItemIndex { get; set; }

        public SwippedItemEventArgs(object sender, SwipeDirection direction)
        {
            Direction = direction;
            Item = sender;
        }
    }
}
