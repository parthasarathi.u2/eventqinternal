﻿namespace EventQMobileApp
{
    public enum DeviceType
    {
        [Description("Unknown")]
        Unknown = 0,
        [Description("iphone")]
        iPhone = 1,
        [Description("android")]
        Android = 2,
        [Description("Other")]
        Other = 9
    }

    public enum NavigationKeys
    {
        [Description("Title")]
        Title,
        [Description("PlaceholderText")]
        PlaceholderText,
        [Description("Page")]
        Page,
        [Description("Attendee")]
        Attendee,
        [Description("Item")]
        Item
    }

    public enum ConnectionTypes
    {
        [Description("Wifi")]
        Wifi,
        [Description("Data")]
        Data,
        [Description("No Connection")]
        NoConnection
    }

    public enum SyncStatus
    {
        [Description("Completed")]
        Completed,
        [Description("Required")]
        Required,
        [Description("InProgress")]
        InProgress,
        [Description("Failed")]
        Failed
    }

    public enum Status
    {
        [Description("Checked-in")]
        Checked_In,
        [Description("Time Not Available")]
        Time_Not_Available,
        [Description("Not Checked In Yet")]
        Not_Checked_In_Yet
    }

    public enum AttendeesType
    {
        [Description("All")]
        All,
        [Description("Delegates")]
        Delegates,
        [Description("Speakers")]
        Speakers,
        [Description("Others")]
        Others
    }

    public enum FiltersType
    {
        [Description("No_Filter")]
        No_Filter,
        [Description("Missing")]
        Missing,
        [Description("Checked_In")]
        Checked_In,
        [Description("VIPs")]
        VIPs
    }

    public enum MessagingCenterValues
    {
        [Description("AttendeeModel")]
        AttendeeModel,
        [Description("AttendeeSelected")]
        AttendeeSelected,
        [Description("Edit")]
        Edit,
        [Description("CurrentEvent")]
        CurrentEvent
    }

    public enum SwipeDirection
    {
        [Description("None")]
        None,
        [Description("Left")]
        Left,
        [Description("Right")]
        Right
    }
}
