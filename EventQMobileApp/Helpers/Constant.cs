﻿namespace EventQMobileApp
{
    public class Constant
    {
        public const string BASE_URL = "http://qmeeto.co/";

	    public const string CANCEL_IMAGE_SOURCE = "Cancel.png";

        public const string FACE_IMAGE_SOURCE = "face.png";

        public const string CHECKED_IMAGE_SOURCE = "checked.png";

        public const string UNCHECKED_IMAGE_SOURCE = "uncheck.png";

        public const string EMPTY = "";
    }
}
