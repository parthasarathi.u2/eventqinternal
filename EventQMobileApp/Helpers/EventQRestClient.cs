using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Polly;
using Polly.Retry;

namespace EventQMobileApp
{
    public class AuthNotReadyException : Exception
    {
        public override string Message => "IdToken not obtained yet!";
    }

    public class EventQRestClient : RestClientHelper
    {
        private bool _requestInProgress;
        HttpStatusCode[] httpStatusCodesToRetry =
            {
                HttpStatusCode.RequestTimeout, // 408
                HttpStatusCode.InternalServerError, // 500
                HttpStatusCode.BadGateway, // 502
                HttpStatusCode.ServiceUnavailable, // 503
                HttpStatusCode.GatewayTimeout // 504
            };

        private readonly AsyncRetryPolicy _httpErrorRetryPolicy = null;
        private readonly AsyncRetryPolicy<bool> _idTokenRetryPolicy = null;

        private AsyncRetryPolicy _resilientPolicy;
        private IAsyncPolicy ResilientPolicy => (_resilientPolicy ?? (_resilientPolicy = _httpErrorRetryPolicy));

        public EventQRestClient(string baseUrl) : base(baseUrl)
        {
            _idTokenRetryPolicy = Policy.HandleResult<bool>(false)
            .WaitAndRetryForeverAsync(retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, delay) =>
            {
                LogUtility.Log($"{nameof(AuthNotReadyException)} - wait and Retry after {delay.Seconds} seconds");
            });

            _httpErrorRetryPolicy = Policy
                .Handle<HttpRequestException>()
                .WaitAndRetryAsync(2,
                  retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
                  (response, delay, retryCount, context) =>
                  {
                      LogUtility.Log($"Will wait and Retry after {delay.Seconds} seconds due to the exception - {response.Message}");
                  });
        }


        private bool PerformReAuth()
        {
            if (_requestInProgress)
            {
                LogUtility.Log("Request in progress. Returning false");
                return false;
            }

            if (Identity.IdToken == null || Identity.TokenExpiry <= DateTime.Now)
            {
                _requestInProgress = true;
                LogUtility.Log($"ReAuth User since token expired at {Identity.TokenExpiry.ToString()}");
            }
            return true;
        }

        private async Task<bool> IsValidToken()
        {
            bool validToken = await Task.FromResult(!Identity.IdToken.IsNullOrEmpty()
                                         && Identity.TokenExpiry >= DateTime.Now);
            if (validToken)
                _requestInProgress = false;
            else
            {
                PerformReAuth();
            }
            return validToken;
        }

        public override async Task<T> GetHttpResponse<T>(string extraParam, List<KeyValuePair<string, string>> headerList = null)
        {
            try
            {
                var valid = true; //await _idTokenRetryPolicy.ExecuteAsync(() => IsValidToken());
                LogUtility.Log($"GET {extraParam} - IdToken is valid ?- {valid}");
                if (valid)
                {
                    return await ResilientPolicy.ExecuteAsync(async () =>
                    {
                        return await base.GetHttpResponse<T>(extraParam, AddAuthHeader(headerList));
                    });
                }
                throw new UnauthorizedAccessException();
            }
            catch (Exception ex)
            {
                await AppInsights.ReportAsync(ex);
                throw;
            }
        }

        public override async Task<T> PostHttpResponse<T>(string extraParam, string data, List<KeyValuePair<string, string>> headerList = null)
        {
            try
            {
                var valid = true; //await _idTokenRetryPolicy.ExecuteAsync(() => IsValidToken());
                LogUtility.Log($"POST {extraParam} - IdToken is valid ?- {valid}");

                if (valid)
                {
                    return await ResilientPolicy.ExecuteAsync(async () =>
                    {
                        return await base.PostHttpResponse<T>(extraParam, data, AddAuthHeader(headerList));
                    });
                }
                throw new UnauthorizedAccessException();
            }
            catch (Exception ex)
            {
                await AppInsights.ReportAsync(ex);
                throw;
            }
        }

        public override async Task<T> PutHttpResponse<T>(string extraParam, string data, List<KeyValuePair<string, string>> headerList = null)
        {
            try
            {
                var valid = await _idTokenRetryPolicy.ExecuteAsync(() => IsValidToken());
                LogUtility.Log($"PUT {extraParam} - IdToken is valid ?- {valid}");
                if (valid)
                {
                    return await ResilientPolicy.ExecuteAsync(async () =>
                    {
                        return await base.PutHttpResponse<T>(extraParam, data, AddAuthHeader(headerList));
                    });
                }
                throw new UnauthorizedAccessException();
            }
            catch (Exception ex)
            {
                await AppInsights.ReportAsync(ex);
                throw;
            }
        }

        public override async Task<T> DeleteHttpResponse<T>(string extraParam, string data, List<KeyValuePair<string, string>> headerList = null)
        {
            try
            {
                var valid = await _idTokenRetryPolicy.ExecuteAsync(() => IsValidToken());
                LogUtility.Log($"DELETE {extraParam} - IdToken is valid ?- {valid}");
                if (valid)
                {
                    return await ResilientPolicy.ExecuteAsync(async () =>
                    {
                        return await base.DeleteHttpResponse<T>(extraParam, data, AddAuthHeader(headerList));
                    });
                }
                throw new Exception("UnAuthorised Access");
            }
            catch (Exception ex)
            {
                await AppInsights.ReportAsync(ex);
                throw;
            }
        }

        private List<KeyValuePair<string, string>> AddAuthHeader(List<KeyValuePair<string, string>> headerList)
        {
            if (headerList == null)
                headerList = new List<KeyValuePair<string, string>>();
            headerList.Add(new KeyValuePair<string, string>("oauth2", Identity.IdToken));
            return headerList;
        }
    }
}
