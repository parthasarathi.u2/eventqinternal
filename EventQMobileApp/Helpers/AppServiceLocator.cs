﻿using DryIoc;

namespace EventQMobileApp
{
    public static class AppServiceLocator
    {
        public static IContainer Container { get; set; }

        public static void IntializeContainer(IContainer container)
        {
            Container = container;
        }

        public static T Resolve<T>()
        {
            var obj = (T)Container.Resolve(typeof(T), IfUnresolved.Throw);
            return obj;
        }
    }
}
