﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace EventQMobileApp
{
    public static class Settings
    {
        static ISettings AppSettings { get => CrossSettings.Current; }

        #region Setting Constants

        const string DeviceIdKey = nameof(DeviceId);
        static readonly string DeviceIdValue = string.Empty;

        public static string DeviceId
        {
            get => AppSettings.GetValueOrDefault(DeviceIdKey, DeviceIdValue);
            set => AppSettings.AddOrUpdateValue(DeviceIdKey, value);
        }

        const string FirstRunKey = nameof(FirstRun);
        public static bool FirstRun
        {
            get => AppSettings.GetValueOrDefault(FirstRunKey, true);
            set => AppSettings.AddOrUpdateValue(FirstRunKey, value);
        }

        #endregion
    }
}
