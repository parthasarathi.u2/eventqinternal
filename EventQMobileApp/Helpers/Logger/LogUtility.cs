﻿using System;
using System.Threading.Tasks;

namespace EventQMobileApp
{
    public static class LogUtility
    {
        private static ILogService _logService;

        public static void Init(ILogService logService)
        {
            _logService = logService;
        }

        public static void Log(string message, VerbosityLevel verbosityLevel = VerbosityLevel.Debug,
            LogCategory logCategory = LogCategory.General)
        {
            Task.Run(() => _logService?.Log(message, verbosityLevel, logCategory));
        }

        public static void Log(Exception ex, VerbosityLevel verbosityLevel = VerbosityLevel.Error,
            LogCategory logCategory = LogCategory.Error)
        {
            Log(ex.ToString(), verbosityLevel, logCategory);
        }
    }
}
