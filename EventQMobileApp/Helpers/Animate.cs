﻿using System;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class Animate
    {
        public Animate()
        {
        }

        public static async void RotateAnimate(View view)
        {
            do
            {
                await view.RotateTo(360, 600);
            } while (true);
        }
    }
}
