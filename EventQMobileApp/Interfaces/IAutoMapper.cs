﻿using System;
using AutoMapper;

namespace EventQMobileApp
{
    public interface IAutoMapper
    {
        IMapper GetMapper();
    }
}
