﻿using SQLite;

namespace EventQMobileApp
{
    public interface IDatabase
    {
        SQLiteConnection DbConnection();

        string GetSqlPath();
    }
}
