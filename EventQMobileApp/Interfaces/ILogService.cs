﻿using System;

namespace EventQMobileApp
{
    public enum VerbosityLevel
    {
        Trace = 0,
        Debug = 1,
        Info = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5,
    }

    public enum LogCategory
    {
        General = 0,
        Screen = 1,
        Action = 2,
        Http = 3,
        Error = 4,
    }

    public interface ILogService
    {
        void Log(string message, VerbosityLevel verbosityLevel = VerbosityLevel.Debug, LogCategory logCategory = LogCategory.General);

        void Log(Exception ex, VerbosityLevel verbosityLevel = VerbosityLevel.Error, LogCategory logCategory = LogCategory.General);
    }
}
