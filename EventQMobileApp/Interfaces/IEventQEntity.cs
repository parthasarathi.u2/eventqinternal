﻿using System;

namespace EventQMobileApp
{
    public interface IEventQEntity
    {
        Guid Id { get; set; }

        bool IsDeleted { get; set; }
    }
}
