﻿using System;
using System.Threading.Tasks;

namespace EventQMobileApp
{
    public interface IAppInsightsService
    {
        Task TrackAsync(string message);

        Task ReportAsync(Exception ex);
    }
}
