﻿using System;
namespace EventQMobileApp
{
    public interface IDisplayable
    {
        string DisplayImage { get; set; }

        string DisplayMessage { get; set; }
    }
}
