﻿using System;
using System.Reflection;
using System.Threading.Tasks;

namespace EventQMobileApp
{
    public interface IMethodDecorator
    {
        void Init(object instance, MethodBase method, object[] args);

        Task OnEntry();

        Task OnExit();

        Task OnException(Exception exception);
    }
}
