﻿namespace EventQMobileApp
{
    public class StandardResponse<T>
    {
        public bool Success { get; set; }
        public string ErrorId { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public T Result { get; set; }

        public static StandardResponse<T> GenericError(string errorMsg, string code = "HY0001")
        {
            return new StandardResponse<T> { Success = false, ErrorCode = code,
                ErrorMessage = errorMsg ?? AppConfig.STANDARD_ERROR_MESSAGE };
        }
    }
}
