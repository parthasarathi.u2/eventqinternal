﻿using SQLite;
using System;
namespace EventQMobileApp
{
    public class BaseAttendee
    {
        [PrimaryKey]
        public Guid AttendeeId { get; set; }

        public DateTime? CheckInTime { get; set; }

        public string SurName { get; set; }

        public string FirstName { get; set; }

        public string AttendeeType { get; set; }

        public string Organisation { get; set; }

        public string AttendeeLabel { get; set; }

        public string Table { get; set; }

        public string SeatNumber { get; set; }

        public bool IsEntranceDone { get; set; }

        public string ContactType { get; set; }

        public bool IsVip { get; set; }

        public string Title { get; set; }

        public string Email { get; set; }
    }

    public class Attendee : BaseAttendee, IEventQEntity
    {
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class ErrorItemInfo : BaseAttendee, IDisplayable
    {
        public string DisplayImage { get; set; }
        public string DisplayMessage { get; set; }

        public static ErrorItemInfo New(bool failure = false)
        {
            return new ErrorItemInfo { DisplayMessage = failure ? Resources.AppText.Failure : Resources.AppText.ErrorText };
        }
    }

    public class NoItemInfo : BaseAttendee, IDisplayable
    {
        public string DisplayImage { get; set; }
        public string DisplayMessage { get; set; }

        public static NoItemInfo New(bool failure = false)
        {
            return new NoItemInfo();
        }
    }
}
