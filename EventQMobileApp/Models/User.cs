﻿using System;
using Newtonsoft.Json;
using SQLite;

namespace EventQMobileApp
{
    public class User : IEventQEntity
    {

        [PrimaryKey,AutoIncrement]
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Token { get; set; }

        public bool IsAuthorized { get; set; }

        public bool IsDeleted { get; set; }
    }
}
