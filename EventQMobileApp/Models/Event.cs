﻿using System;
using System.Runtime.CompilerServices;
using Prism.Mvvm;
using SQLite;

namespace EventQMobileApp
{
    public class BaseEvent : BindableBase
    {
        private int _absentCount;

        public int AbsentCount
        {
            get => _absentCount;
            set => SetProperty(ref _absentCount, value);
        }

        private int _checkedInCount;

        public int CheckedInCount
        {
            get => _checkedInCount;
            set => SetProperty(ref _checkedInCount, value);
        }

        [PrimaryKey, NotNull]
        public int EventId { get; set; }

        private int _registeredCount;

        public int RegisteredCount
        {
            get => _registeredCount;
            set => SetProperty(ref _registeredCount, value);
        }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string Country { get; set; }

        public string EventName { get; set; }

        public string EventRegistrationLink { get; set; }

        public string Location { get; set; }

        public string TimezoneName { get; set; }

        public string VenueName { get; set; }

        public TimeSpan? StartTime { get; set; }

        public TimeSpan? EndTime { get; set; }


    }
    public class Event : BaseEvent , IEventQEntity
    {
        private double _progress;
        [Ignore]
        public double Progress
        {
            get => _progress;
            set
            {
                SetProperty(ref _progress, value);
                Percentage = (int)(_progress * 100);
            }
        }

        private int _percentage;
        [Ignore]
        public int Percentage
        {
            get => _percentage;
            set => SetProperty(ref _percentage, value);
        }

        private SyncStatus _syncSate;
        [Ignore]
        public SyncStatus SyncState
        {
            get => _syncSate;
            set => SetProperty(ref _syncSate, value);
        }

        private bool _isSelected;
        [Ignore]
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        [AutoIncrement,PrimaryKey]
        public Guid Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? LastModified { get; set; }

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName.Equals(nameof(CheckedInCount)) || propertyName.Equals(nameof(RegisteredCount)))
            {
                if (RegisteredCount > 0)
                    Progress = (double)CheckedInCount / RegisteredCount;
            }
        }

    }

}
