﻿using System;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class EventTemplateSelector : DataTemplateSelector
    {
        DataTemplate _eventTemplate;

        public EventTemplateSelector()
        {
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    _eventTemplate = new DataTemplate(typeof(Droid_EventViewCell));
                    break;
                case Device.iOS:
                    _eventTemplate = new DataTemplate(typeof(IOS_EventViewCell));
                    break;
            }
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if(item is Event)
            {
                switch (Device.RuntimePlatform)
                {
                    case Device.Android:
                        _eventTemplate.SetValue(Droid_EventViewCell.ParentBindingContextProperty, container.BindingContext);
                        break;
                    case Device.iOS:
                        _eventTemplate.SetValue(IOS_EventViewCell.ParentBindingContextProperty, container.BindingContext);
                        _eventTemplate.SetValue(IOS_EventViewCell.DestructiveColorProperty, Application.Current.Resources["ThemeColor"]);
                        break;
                }
                return _eventTemplate;
            }
            return null;
        }
    }
}
