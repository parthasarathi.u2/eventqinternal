﻿using System;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class AttendeeTemplateSelector : DataTemplateSelector
    {
        readonly DataTemplate _attendeeTemplate;
        readonly DataTemplate _emptyTemplate;
        readonly DataTemplate _errorTemplate;

        public AttendeeTemplateSelector()
        {
            _attendeeTemplate = new DataTemplate(typeof(AttendeeViewCell));
            _emptyTemplate = new DataTemplate(typeof(NoInfoCell));
            _errorTemplate = new DataTemplate(typeof(ErrorCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            if (item is AttendeeBaseViewModel)
            {
                return _attendeeTemplate;
            }
            if (item is ErrorItemInfo)
            {
                return _errorTemplate;
            }
            if (item is NoItemInfo)
            {
                return _emptyTemplate;
            }
            return null;
        }
    }
}
