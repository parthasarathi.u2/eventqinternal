﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class RotateAnimationBehavior : Behavior<View>
    {

        protected override void OnAttachedTo(View bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.BindingContextChanged += Bindable_BindingContextChanged;
        }

        private async void Bindable_BindingContextChanged(object sender, EventArgs e)
        {
            View view = (View)sender;
            await Task.WhenAll(
                view.ScaleTo(1.2, 250),
                view.RotateYTo(360, 600, Easing.Linear)
               );
            await view.ScaleTo(1, 250, Easing.Linear);
            await view.RotateYTo(0, 0);
        }

        protected override void OnDetachingFrom(View bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.BindingContextChanged += Bindable_BindingContextChanged;

        }
    }
}
