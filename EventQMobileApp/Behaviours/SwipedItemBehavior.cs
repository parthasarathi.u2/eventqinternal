﻿using System;
using Xamarin.Forms;

namespace EventQMobileApp
{
    public class SwipedItemBehavior : Behavior<ListView>
    {
        public SwipedItemBehavior()
        {
        }

        protected override void OnAttachedTo(ListView bindable)
        {
            bindable.ItemTapped += OnItemSwiped;
        }

        private void OnItemSwiped(object sender, ItemTappedEventArgs e)
        {
        }

        protected override void OnDetachingFrom(ListView bindable)
        {
            bindable.ItemTapped -= OnItemSwiped;
        }
    }
}
