# EventQ Mobile Application | iOS | Android

A Xamarin.Forms app for EventQ's clients and Event cooridinators.

Event check-in app lets you manage RSVP invitations. With the EventQ app you can check in guests 10x faster than with paper lists! EventQ is a cross platform mobile application, developed using Xamarin.Forms in both Android and iOS platforms.

### Coding best practices
- No hard coding, use constants/configuration values.
- Group similar values under an enumeration (enum).
- Comments – Do not write comments for what you are doing, instead write comments on why you are doing. Specify about any hacks, workaround and temporary fixes. Additionally, mention pending tasks in your to-do comments, which can be tracked easily.
- Avoid multiple if/else blocks.
- Use framework features, wherever possible instead of writing custom code.

### Naming Conventions
- Pages should be suffixed with page. 
```c#
MobileAttendeeDetailsPage
HelpPage
SettingsPage
```
- ViewModels that directly handle the business logic of a page will be suffixed ViewModel
```
MobileAttendeeDetailsPageViewModel
HelpPageViewModel
SettingsPageViewModel
```
- Start private fields with an underscore
```c#
private int _count;
private readonly string _myString;
```

- Parameter names will be in Camel Case.
```c#
public void MyMethod(string paramOne, int paramTwo) 
{

}
```

- Method names will be in Pascal Case
```c#
private void MethodOne() {

}

private int ThisMethodReturnsAnInt {

}
```

- Public properties should be in Pascal case
```c#
public bool IsBusy {get; set;}
public string InjuryArea {get; set;}
```

Happy Coding !
